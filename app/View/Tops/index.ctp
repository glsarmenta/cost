<div class = 'full'><!-- <img src="/Cost/img/bg.jpg"> --><div class="container">
      <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="/img/slide1.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
             <!--  <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and                right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/img/caro1.jpeg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>Shop now!</h1>
              <p>COST, designed for the modern and empowered Filipino and Japanese.Our products: COST clothes, a fresh take on reversible, fashion-forward yet affordable clothing.</p>-->
              <p><a class="btn btn-lg btn-primary" href="/Tops/contacts" role="button">About COST</a></p>
            </div>
          </div>
        </div>

      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-6">
          <img class="img-circle" src="/img/clothes.jpeg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Male</h2>
          <p class="text-justify">Get the latest styles in men's clothing right here online at COST. From the most current trends right down to your everyday style essentials, we update our site daily with the latest in men's fashion such as jeans, suits, t-shirts and lots, lots more. Shop men’s clothes now with COST.</p>
          <p><a class="btn btn-default" href="/Tops/Male" role="button">Shop Male &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-6">
          <img class="img-circle" src="/img/image1xl.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Female</h2>
          <p class="text-justify">Looking at recent trends in womens fashion, one will realize how much it has changed in the past few years. Designers are becoming more experimental and creative when it comes to necklines, cuts, and fabrics. This has resulted to a lot of new and very exciting designs.</p>
          <p><a class="btn btn-default" href="/Tops/Female" role="button">Shop Female &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">COST White Roller Crew Neck<span class="text-muted"> Polo Shirt</span></h2>
          <p class="lead">Our roller fit t-shirts feature more room in the body with lower roll sleeves and a longer body length. Ideal if you wanted your plain casual style to have a more trend-led shape. White roller crew t-shirt.</br></br>100% cotton</br>Measured from Medium</br>Machine wash cold</br>Imported</br></br>Php 1000.00</p>
        </div>
        <div class="col-md-5">
          <img class="img-circle" src="/img/COST White Roller Crew Neck T-Shirt.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="img-circle" src="/img/COST Off Shoulder Peplum Top.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">COST Off Shoulder<span class="text-muted"> Peplum Top</span></h2>
          <p class="lead">Less is definitely more with this Off Shoulder Peplum Top by Something Borrowed. Featuring neatly placed darted details with half-peplum skirting, trust this top to accentuate your waist while showing off a little shoulder.</br></br>97% polyester</br>Measured from Small</br>Hand wash cold</br></br>Php 800.00</p>
        </div>
      </div>

 </div>   
 </div></div>
<?php 
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
echo $ip;
?>
