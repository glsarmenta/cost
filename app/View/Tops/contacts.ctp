 <div class="container">

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post text-justify">
          <h1>About</h1>
           <br>In 2013, COST Corporation, headed by Abundio, Hideshi, Godfrey and Denmark, opened the first COST store in Adamson Fashion University. Since then, the brand has evolved from a chain of roadside stores to an international leader in style, quality, and fun. It doesn’t matter who you are or where you live, COST makes clothes that transcend all categories and social groups. Our clothes are made for all, going beyond age, gender, occupation, ethnicity, and all other ways that define people. Our clothes are simple and essential yet universal, so people can freely combine them in their own unique style. As our four founder says, “COST clothes are Comfortable Original Stylish Trends and highly finished elements of style in clothes that suit your values wherever you live. This unique concept of clothes sets us apart from apparel companies whose sole purpose is the pursuit of fashion trends.” the four founders. E-commerce fashion (Clothes and Shoes). The purpose of the business is to sell outdoor clothing and shoes online. We will be offering outdoor clothing for almost every type of active use. Aside from that our product will also focus on the seasonal clothing demands of our customers. COST, designed for the modern and empowered Filipino and Japanese. Our products: “COST clothes,” a fresh take on reversible, fashion-forward yet affordable clothing; COST caps, a specialized headwear design company that has now diversified into a full street wear line; and COST Sole, a shoe with the goal of helping the environment by using recycled material for their fashionable and comfortable footwear.<br>
           <br><strong>Mission Statement</strong><br><br>Our mission is to provide complete clothing and shoes online shopping with an unparalleled selection of fashion on convenient environment. Providing full services with an emphasis on convenient hours and easy to bargain shop from the comfort of their home or office.<br>
           <br><strong>Ownership of Business</strong><br><br>The business will be formed as a COST corporation selling clothes and shoes with four stockholders: Abundio, Hideshi, Godfrey and Denmark. Each stockholder will own 25 % of the issued stocks and equal to their individual paid – in capital. The clothes and shoes brand name is COST. The formal agreement and charter will be in accordance with the laws of the Republic of the Philippines and drawn up by Atty. Jorge Sacdalan.
 <br>
           <h1>Contact Us</h1>
                       COST is located in an easily accessible place in Manila at One Adriatico Place Tower I, Adriatico Street, Ermita, Manila (Main Branch) owned by Mr. Hideshi Ogoshi and at Imus Cavite (Branch) owned by Mr. Godfrey L. Sarmenta. The business hours will be from 8:00 AM to 10:00 PM. Monday to Sunday. The main branch and the second branch are open 7 days a week. Our main branch and second branch are usually closed on Philippine National Holidays such as: New Year’s Day (January 1), Good Friday, All November 1 and December 25. Please refer to the specific branch as to operating hours.<br>
                       <br>
           <address>
            <strong>Cost, Inc.</strong><br>
            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> costincorporated@gmail.com<br>
           <span class="glyphicon glyphicon-phone" aria-hidden="true"></span><abbr title="Phone">P:</abbr> 02) 428-9136 (Main Branch)<br>
            <span class="glyphicon glyphicon-phone" aria-hidden="true"></span><abbr title="Phone">P:</abbr>(046) 970-4752 (Cavite Branch)<br>
        
          </address>

          
          
          </div><!-- /.blog-post -->

          

        </div><!-- /.blog-main -->

        
          
            
          
      

    </div><!-- /.container -->