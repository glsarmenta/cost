<div class="container">
	<div class="row">
		<div class="col-xs-8">

			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
						<div class="row">
							<div class="col-xs-6">
								<h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
							</div>
							<div class="col-xs-6">
								<a class="btn btn-primary btn-sm btn-block" href="/Tops"><span class="glyphicon glyphicon-share-alt"></span> Continue shopping</a>
									
								
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><form id='cartel'>
				<?php $b = $data;
				$o = 0;
				foreach($data as $data)
				
				foreach($data as $info=>$key){ 

					?>

					<div class="row">
						<div class="col-xs-2"><img class="img-responsive" src=<?php echo '"/files/Products4/'.$key['product_images']['file_name'].'"';?>>
						     <span class="label label-default">Fitting: <?php echo $key['SizeName']; ?></span>
						</div>
						<div class="col-xs-4">
							<h4 class="product-name"><strong><?php echo $key['Product']['name'];?></strong></h4>
						</div>
						<div class="col-xs-6">

							<div class="col-xs-6 text-right">
								<h6><strong><?php echo $key['selling_prices']['price'];?><span class="text-muted"> x</span></strong></h6>
							</div>
							<div class="col-xs-4">
								<input type="text" name="item[]" data-price=<?php echo $key['selling_prices']['price'];?> id="qty" class="form-control input-sm" value="1"><?php echo $key['selling_prices']['price'] ?>
								<input type="hidden" name="sizes[]" value="<?php echo $key['SizeName']; ?>" />
							</div>
							<div class="col-xs-2">
							<a class="btn btn-link btn-xs" href="/Tops/deleteitem/<?php echo $o; ?>"><span class="glyphicon glyphicon-trash"> </span></a>
								
							</div>
						</div>
					</div>
					<hr> <?php $o++; } ?>

					
					<div class="row">
						<div class="text-center">
							<div class="col-xs-9">
								<h6 class="text-right">Added items?</h6>
							</div>
							<div class="col-xs-3">
								<button type="button" class="btn btn-default btn-sm btn-block disabled">
									Update cart
								</button>
							</div>
						</div>
					</div></form>
				</div>
				<div class="panel-footer">
					<div class="row text-center">
						<div class="col-xs-9">
							<h4 class="text-right">Total <strong><span id="total"><?php 
							$priceinfo = array();
							
							foreach($b as $b)
							foreach($b as $z=>$v)
							{	
								array_push($priceinfo , $v['selling_prices']['price']);
							}
							
						    //echo round(array_sum($priceinfo),2);
						     echo array_sum($priceinfo);
							 ?></span></strong>
							 <input type="hidden" id="hiddenTotal" name="total" data-value="<?php echo array_sum($priceinfo); ?>"></h4>
						</div>
						<div class="col-xs-3">
						<a id="checkout" class="btn btn-success btn-block" href="#"><span class="glyphicon glyphicon-share-alt"></span> Checkout</a>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script type="text/javascript">

$(document).ready(function(){
	
var form = $("#cartel");

$('body').on("click", "a[id=checkout]", function (){
	event.preventDefault();
    


      var url = '/Tops/checkLog'; // the script where you handle the form input.

      $.ajax({
             type: "POST",
             url: url,
             data:form.serialize(),
             
             success: function(data)
             {
             	if(data == 1){
             		console.log('success');
                
				    window.location.href = "/Tops/thank";
	
             	}else{
             		alert(data);
             	}
             	
             }

           });
      // window.location.href = "/Tops/thank";

    });

  });
$('body').on("keyup", "input[id=qty]", function (){
    

    if($(this).val()[0]==0 ){
    	
    	
    	$(this).prop('value',$(this).val().substring(1));
    	console.log($(this).val().substring(1));
    	// $(this).slice(1, -1);
    	// console.log($(this).val());
    }
        if($(this).val() == '' )
    {
    	$(this).prop('value',0);
    }

    if($(this).val()>=0 ) {

    if(Math.floor($(this).val()) == $(this).val() && $.isNumeric($(this).val())) 
    {
    	var y = $('#hiddenTotal').attr('data-value');
    	var x =$(this).attr('data-price');
    	var z = $(this).val();

    	 y = x - y;
    	 y =  (z * x) - y;
    	 $('#total').empty();
    	 $('#total').append(parseFloat(y).toFixed(2));

    	 var total = parseFloat(y).toFixed(2);
    	  $('#hiddenTotal').val(total);
    	
    	
    }

}

        });

</script>
