

    <div class="container">
        <!-- Page Heading -->
        <?php foreach($data as $info){ ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $info['Product']['name']; ?>
                   
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Project One -->
        <div class="row">
            <div class="col-md-6 thumbnail">
                <a href="#">
                    <img class="img-responsive" style =" display:block;
                            height:400px;
                            width:auto;" src=<?php echo '"/files/Products4/'.$info['product_images']['file_name'].'"';?>  alt="">
                </a>
                <span>Fitting: <?php if($info['stocks']['Small']!=0){?>
                <span class="label label-default">Small</span><?php }?>
                <?php if($info['stocks']['Large']!=0){?>
                <span class="label label-default">Large</span><?php }?>
                <?php if($info['stocks']['XLarge']!=0){?>
                <span class="label label-default">X-Large</span><?php }?>
                </span>
            </div>

            <div class="col-md-5">
            <form id='cart'>
                <h3><?php echo $info['Product']['name']; ?></h3>
                <h4><span class="label label-info"><input type="hidden" name="prize" value="<?php echo $info['selling_prices']['price']; ?>"><input type="hidden" name="productid" value="<?php echo $info['Product']['id']; ?>"><?php echo $info['selling_prices']['price']; ?></span></h4>
                <p><?php echo $info['Product']['description']; ?></p>
                <div class="col-md-7">
                
                <select class="form-control" name='size'>
                <?php if($info['stocks']['Small']!=0){?>
                <option name='size[]' value="1">Small</option>
                <?php }?>
                <?php if($info['stocks']['Large']!=0){?>
                <option value="2" name='size[]'>Large</option><?php }?>
                 <?php if($info['stocks']['XLarge']!=0){?>
                <option value="3" name='size[]'>X-Large</option><?php } ?>
                </select><br><a class="btn btn-primary" id = "buy" data-product="<?php echo $info['Product']['id']; ?>" href="#"><span id="buyId">Cart</span> <span class="glyphicon glyphicon-chevron-right"></span></a>
            </form>

                </div>
                 <!-- <div class="col-lg-3">
                <select class="form-control" name='qty'>
                <option name='qty[]' value="1">1</option>
                <option name='qty[]' value="2">2</option>
                <option name='qty[]' value="3">3</option>
                <option name='qty[]' value="4">4</option>
                <option name='qty[]' value="5">5</option>
                <option name='qty[]' value="6">6</option>
                <option name='qty[]' value="7">7</option>
                <option name='qty[]' value="8">8</option>
                <option name='qty[]' value="9">9</option>
                <option name='qty[]' value="10">10</option>
                
                </select>
                  </div> -->
              </div>
        </div>
    <?php } ?>
        <!-- /.row -->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

 <script type="text/javascript">

$(document).ready(function(){

$('body').on("click", "a[id=buy]", function (){
  event.preventDefault();
    
var small = <?php echo $data[0]['stocks']['Small']; ?>;
var large = <?php echo $data[0]['stocks']['Large']; ?>;
var xlarge = <?php echo $data[0]['stocks']['XLarge']; ?>;


      var url = '/Tops/addCart'; // the script where you handle the form input.

      $.ajax({
             type: "POST",
             url: url,
             data:$('#cart').serialize(),
             
             success: function(data)
             {
                console.log(data);
               if(data == 1){
                
                 
                $('#buyId').empty();

                $('#buyId').append('added to cart');
                $('#checkoutnumber').empty();
                $('#checkoutnumber').load('getcheckout #checkoutnumber');
                  //window.location.href = "/Tops/thank";
                 // show response from the php script.
               }
               else{
                alert(data);
               }
             

             }

           });

    });

  });
</script>

