        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


          <h2 class="sub-header">Product Inventory 
          </h2><h3><span id="updateHeader" class="label label-danger"></span></h3>
          <div class="table-responsive">
          
            <table id = "tableProducts" class="table table-striped">
              <thead>
                <tr>
                <th>Image</th>
                  <th>Stocks</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Details</th>
                  
                </tr>
              </thead>
              <tbody>
              <div style="height:0px;overflow:hidden">
   <input type="file" id="fileInput" name="fileInput" />
</div>


<script>
   function chooseFile() {
      $("#fileInput").click();
   }
</script>
              <?php foreach($data as $info){?> 
                <tr>
                <td><a href="#" onclick="chooseFile();">
                    <img class="img-responsive" style =" display:block;
                            height:50px;
                            width:auto;" src=<?php echo '"/files/Products4/'.$info['pi']['file_name'].'"';?>  alt="">
                </a></td>
                  <td><?php 

                  if($info['stocks']['stocks']<=20){ ?>
                  <span class="label label-danger"><?php echo $info['stocks']['stocks']; ?></span>
                  <?php
                  }else{ ?>
                  <span class="label label-primary"><?php echo $info['stocks']['stocks']; ?></span><?php } ?>
                  </td>
                  <td><?php echo $info['products']['id']; ?></td>
                  <td><a href="#" data-toggle="modal" data-target="#exampleModal" 
                   data-whatever="<?php echo $info['products']['name']; ?>"><?php echo $info['products']['name']; ?></a>
                   </td>
                  <td><?php echo $info['products']['created']; ?></td>
                </tr> <?php } ?>
              </tbody>
            </table>
<!--           <div class="pagination pagination-large">
    <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
    </div> -->
          </div>
        </div>
      </div>
    </div>





<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Information of</h4>
      </div>
      <div class="modal-body">
        <form id ="ProductInformation">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Product Name:</label>
            <input type="text" name="Productname" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Details:</label>
            <textarea class="form-control" name="Description" id="message-text"></textarea>            
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Price:</label>
            <input type="text" name="Price" class="form-control" id="price">          
          </div>

                    <div class="form-group">
            <label for="message-text" class="control-label">Sizes:</label><br/>
            <input type="hidden" name="Stock" class="form-control" id="stock">
            Small:<br/>
            <input type="text" name="Small" class="form-control" id="Small">          
            Large:<br/>
            <input type="text" name="Large" class="form-control" id="Large">       
            X-Large:<br/>
            <input type="text" name="X-Large" class="form-control" id="X-Large"> 
           
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="Update" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
$('#Overview').attr('class','active');
$(document).ready(function(){
  var prodId = '';

  $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) ;// Button that triggered the modal
  var recipient = button.data('whatever'); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);

      $.ajax({
          type: "POST",
           url: '/Admins/get_product/'+recipient,
           dataType: "JSON",
           success:function(data) {
         // console.log(data['products']['name']) ;
          console.log(data);
          modal.find('#message-text').val(data['products']['description']);
          modal.find('#price').val(data['selling_prices']['price']);
          modal.find('#stock').val(data['stocks']['stocks']);
          modal.find('#Small').val(data['stocks']['Small']);
          modal.find('#Large').val(data['stocks']['Large']);
          modal.find('#X-Large').val(data['stocks']['XLarge']);

          prodId =  data['products']['id'];

      }
      
   });

  modal.find('.modal-title').text('Information of ' + recipient);
  modal.find('.modal-body input').val(recipient);
  
  

});


  $("#Update").click(function() {

      var url = '/Admins/updateProduct/' +prodId; // the script where you handle the form input.

      $.ajax({
             type: "POST",
             url: url,
             data: $("#ProductInformation").serialize(), // serializes the form's elements.
             success: function(data)
             {
              console.log(data);

                 // show response from the php script.
                 $('#updateHeader').empty();
                 $('.table-responsive').empty();
                 $('.table-responsive').load('/Admins .table-responsive');

                 $('.close').trigger('click');
                 $('#updateHeader').prepend(data);

             }

           });

      // avoid to execute the actual submit of the form.
  });
});
</script>