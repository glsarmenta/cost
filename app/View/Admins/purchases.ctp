        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">Top Sales</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src=<?php echo '"/files/Products4/'.$TopM[0]['product_images']['file_name'].'"';?> class="img-responsive"  style ="
                            height:200px;
                            width:auto;" alt="Generic placeholder thumbnail">
              <h4>Mens</h4>
              <span class="text-muted">Total sales : <?php 
                echo @$TopM[0]['0']['qSum'];?> for <?php 
                echo @$TopM[0]['products']['name'];?></span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
            <img src=<?php echo '"/files/Products4/'.$TopF[0]['product_images']['file_name'].'"';?> class="img-responsive"  style ="
                            height:200px;
                            width:auto;" alt="Generic placeholder thumbnail">
              <h4>Womens</h4>
              <span class="text-muted">Total sales : <?php echo @$TopF[0]['0']['qSum'];?> for <?php 
                echo @$TopF[0]['products']['name'];?></span>
            </div>

            
          </div>


          <h2 class="sub-header">Purchases 
          </h2><h3><span id="updateHeader" class="label label-danger"></span></h3>
          <div class="table-responsive">
          
            <table id = "tableProducts" class="table table-striped">
              <thead>
                <tr>
                <th>Date</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Details</th>
                  
                </tr>
              </thead>
              <tbody>
             
             	<?php foreach($orders as $info){ ?>
             	<tr>
             		<td><?php echo@$info['OrderItems']['created']; ?> </td>
             		<td><?php echo@$info['products']['name']; ?></td>
             		<td><?php echo@$info['selling_prices']['price'] * @$info['OrderItems']['amount_of_order']; ?> </td>
             		<td><?php echo@$info['users']['first_name'].' '.@$info['users']['middle_name'].' '.@$info['users']['last_name']; ?> </td>
             		</tr>
             		<?php } ?>

             
          </table><!-- /.container -->
          </div>
        </div>
      </div>
    </div>





          