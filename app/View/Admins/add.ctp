
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         <div class="container">
    <div class="row">

           <?php echo $this->Form->create('products',
           array('url/'=>$this->Html->url(array('controller'=>'/Admins', 'action'=>'/add')),'enctype' => 'multipart/form-data','type'=>'file'
            )
           ); ?></h2><h3><span id="updateHeader" class="label label-danger"><?php echo $prodSuc;?></span></h3>
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong>
                </div>
                <h3><span id="updateHeader" class="label label-danger"></span></h3>
                  <div class="form-group">
                    <label for="Name">Name</label>
                    <div class="input-group"><?php echo $this->Form->input('name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Name','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('products.name'); ?>
                    
                </div>
                <div class="form-group">
                    <label for="Name">Price:</label>
                    <div class="input-group"><?php echo $this->Form->input('price',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter price','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('products.price'); ?>
                    
                </div>
                <div class="form-group">
                    <label for="InputEmail">Categories</label>
                    <div class="input-group">
                        <select name ="data[products][product_categories_id]"  class="form-control">
                          <?php foreach($product_categories as $product_categories){?>
                          <option value=<?php echo '"'.$product_categories['product_categories']['id'].'"'; ?> id=<?php echo '"'.$product_categories['product_categories']['id'].'"'; ?> ><?php echo $product_categories['product_categories']['type']; if($product_categories['product_categories']['gender']==1){echo
                            ' (Male)'; } else{ echo ' (Female)'; };?></option> <?php } ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputName">Description</label>
                    <div class="input-group">
                       <textarea class="form-control" name ="data[products][description]" rows="3"></textarea>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                                        <?php 
                         echo $this->Form->error('products.description'); ?>
                </div>
              <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <!-- <input type='date' name ="data[products][release_date]"   id = "datepicker"class="form-control" /> -->
                    <input class="datepicker form-control" id="datepicker"name ="data[products][release_date]" data-date-format="mm/dd/yyyy">
                    <span class="input-group-addon"><span id="calendar" class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

                                        <?php 
                         echo $this->Form->error('products.release_date'); ?>
            </div>
                <div class="form-group">
                    <label for="InputEmail">Supplier</label>
                    <div class="input-group">
                        <select name ="data[products][supplier_id]"  class="form-control">
                          <?php foreach($supplier as $supplier){?>
                          <option value=<?php echo '"'.$supplier['suppliers']['id'].'"'; ?> id=<?php echo '"'.$supplier['suppliers']['id'].'"'; ?> ><?php echo $supplier['suppliers']['name']; ?></option> <?php } ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                <img class="img-circle" src="" alt="Generic placeholder image" style="width: 140px; height: 140px;">
                <div class='input-group date'>
                    <?php echo $this->Form->input('productsFile',array('type'=>'file','id'=>'productsFile','class'=>'form-control' ,'div'=>false,'label'=>false)); ?>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-upload"></span>
                    </span>
                     
                </div>
                 <?php echo $this->Form->error('products.file'); ?>
            </div>
               
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right"><?php echo $this->Form->end(); ?>
            
       
</div><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


  <script>
  var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
  $('#datepicker').datepicker({
    minDate: today 
})
  

  </script>

 <script type="text/javascript">
$('#liAddProducts').attr('class','active');
$(document).ready(function(){


  $('#calendar').click(function(){
    $('.datepicker').trigger('click');

    });
    $('#submit').click(function(){

   if($('#productsFile').val()==''){
    event.preventDefault();
    alert('empty');
   }
   else{
    $('#send').trigger('click');
   }
    });



  $(function() {
       $("input:file").change(function (){
         var fileName = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
          readURL(this);
         //$("").html(fileName);
         var ext = this.value.match(/\.(.+)$/)[1];
      switch (ext) {
        case 'jpg':
        case 'JPG':
        case 'jpeg':
        case 'JPEG':
        case 'png':
        case 'PNG':
        case 'gif':
        case 'GIF':

            $('#productsFile').attr('disabled', false);
            $('.img-circle').attr('src',fileName);
            break;
        default:
            alert('This is not an allowed file type.');
            this.value = '';
    }
       });

    });
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.img-circle').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


});
</script>
            
          </div>

