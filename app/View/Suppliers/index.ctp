
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         <div class="container">
    <div class="row">

           <?php echo $this->Form->create('Suppliers',array('url/'=>$this->Html->url(array('controller'=>'/Admins', 'action'=>'/add')))); ?>
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong>
                </div><h3><span id="updateHeader" class="label label-danger"><?php echo $Update; ?></span></h3>
                
                  <div class="form-group">
                    <label for="Name">Suppliers's Name</label>
                    <div class="input-group"><?php echo $this->Form->input('name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Name','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('Suppliers.name'); ?>
                </div>
              <div class="form-group">
                    <label for="Name">Address</label>
                    <div class="input-group"><?php echo $this->Form->input('address',
                    array('class'=>'form-control',
                    'placeholder'=>'Address','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('Suppliers.address'); ?>
                    
                </div>
                   <div class="form-group">
                    <label for="Name">Email</label>
                    <div class="input-group"><?php echo $this->Form->input('email',
                    array('class'=>'form-control',
                    'placeholder'=>'Email','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('Suppliers.email'); ?>
                    
                </div>
                    <div class="form-group">
                    <label for="Name">Suppliers's Contact</label>
                    <div class="input-group"><?php echo $this->Form->input('phone_number',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Contact','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('Suppliers.phone_number'); ?>
                    
                </div>
                             
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right"><?php echo $this->Form->end(); ?>
            
        
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
  $('#liAddSupplier').attr('class','active');
  $('#TopAddSupplier').attr('class','active');
  
</script>
