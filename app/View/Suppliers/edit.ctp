      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    
       <h2 class="sub-header">Product Inventory 
          </h2><h3><span id="updateHeader" class="label label-danger"></span></h3>
          <div class="table-responsive"><a href="/Suppliers">Add User</a>
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Contact</th>
                  
                </tr>
              </thead>
              <tbody id="Suppliers">
              <?php foreach($data as $info){?> 
                <tr>
                  <td><?php echo $info['Suppliers']['id']; ?></td>
                  <td><a href="#" data-toggle="modal" data-target="#exampleModal" 
                   data-whatever="<?php echo $info['Suppliers']['name']; ?>"><?php echo $info['Suppliers']['name']; ?></a>
                   </td>
                  <td><?php echo $info['Suppliers']['address']; ?></td>
                  <td><?php echo $info['Suppliers']['email']; ?></td>
                  <td><?php echo $info['Suppliers']['phone_number']; ?></td>
                </tr> <?php } ?>
              </tbody>
            </table>
      <div class="pagination pagination-large">
    <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
    </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Information of</h4>
      </div>
      <div class="modal-body">
        <form id ="ProductInformation">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Name:</label>
            <input type="text" name="name" class="form-control" id="recipient-name">
          </div>

          <div class="form-group">
            <label for="message-text" class="control-label">Address</label>
            <input type="text" name="address" class="form-control" id="price">          
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Telephone</label>
            <input type="text" name="phone_number" class="form-control" id="stock">          
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="Update" class="btn btn-primary">Edit</button>
        <button type="button" id="Delete" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var prodId = '';
  $('#liEditSupplier').attr('class','active');
  $('#TopAddSupplier').attr('class','active');

  $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) ;// Button that triggered the modal
  var recipient = button.data('whatever'); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);

      $.ajax({
          type: "POST",
           url: '/Suppliers/get_supplier/'+recipient,
           dataType: "JSON",
           success:function(data) {
         // console.log(data['products']['name']) ;
         
          modal.find('#message-text').val(data['Suppliers']['name']);
          modal.find('#price').val(data['Suppliers']['address']);
          modal.find('#stock').val(data['Suppliers']['phone_number']);
          prodId =  data['Suppliers']['id'];
          console.log(data);

      }
      
   });

  modal.find('.modal-title').text('Information of ' + recipient);
  modal.find('.modal-body input').val(recipient);
  
  

});


  $("#Update").click(function() {

      var url = '/Suppliers/update_Supplier/' +prodId; // the script where you handle the form input.

      $.ajax({
             type: "POST",
             url: url,
             data: $("#ProductInformation").serialize(), // serializes the form's elements.
             success: function(data)
             {

                 // show response from the php script.
                 $('#updateHeader').empty();
                 $('.table-responsive').empty();
                 $('.table-responsive').load('edit .table-responsive');

                 $('.close').trigger('click');
                 $('#updateHeader').prepend(data);

             }

           });

      // avoid to execute the actual submit of the form.
  });

  $("#Delete").click(function() {

      var url = '/Suppliers/delete_Supplier/' +prodId; // the script where you handle the form input.

      $.ajax({
             type: "POST",
             url: url,
             data: $("#ProductInformation").serialize(), // serializes the form's elements.
             success: function(data)
             {

                 // show response from the php script.
                 $('#updateHeader').empty();
                 $('.table-responsive').empty();
                 $('.table-responsive').load('edit .table-responsive');

                 $('.close').trigger('click');
                 $('#updateHeader').prepend(data);

             }

           });

      // avoid to execute the actual submit of the form.
  });
});
</script>