<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>

<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		// echo $this->Html->css('bootstrap.min.css');
	 //    echo $this->html->script('bootstrap.min.js');

	    
	    // echo $this->html->script('doc.min.js');
	    // echo $this->html->script('bug10.js');
	    // echo $this->html->css('carousel.css');


		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


	?>
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/carousel.css" rel="stylesheet">

</head>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/Tops">Cost</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/Tops/male">Male <span class="sr-only">(current)</span></a></li>
        <li><a href="/Tops/female">Female</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/Tops/Contacts">About Us</a></li>
            <li><a href="/Settings/registration">Register</a></li>
            
            <!-- <li class="divider"></li>
            <li><a href="">Account</a></li> -->
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search" method="get" action="/Search/index">
        <div class="form-group">
          <input type="text" class="form-control" id="searchForm" placeholder="Search" name="words">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
         <a class="btn btn-default" href="/Tops/getcheckout"><span id="checkoutnumber"><?php 
         $cart = $this->Session->read('itemCart');
         if($cart != null){
         
         echo count($cart);} else { echo '0'; } ?></span>
          <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> </a>  
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><?php if(AuthComponent::user('name')==NUll){ ?><a href="/Settings/login">Login</a> <?php } else{?>
        <a href="/Settings/logout"><?php echo AuthComponent::user('name').' (logout)'?></a><?php } ?></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav><body>
<?php echo $this->fetch('content'); ?>
</body>
		
		<hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Cost, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    <!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    
    <script src="/js/typeaheads.js"></script>
    <script src="/js/bug10.js"></script>
    
  
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    

</html>
