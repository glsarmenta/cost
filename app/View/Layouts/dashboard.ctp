<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>

<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		// echo $this->Html->css('bootstrap.min.css');
	 //    echo $this->html->script('bootstrap.min.js');

	    echo $this->html->script('jquery.min.js');
	    // echo $this->html->script('doc.min.js');
	    // echo $this->html->script('bug10.js');
	    // echo $this->html->css('carousel.css');


		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


	?>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    
  </head>
  
   <body>
        <nav class="navbar navbar-default navbar-fixed-top">
          <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Cost</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li id="TopAddProducts" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/Admins/Add">Add Products</a></li>
            <li><a href="/Admins/edit">Edit Suppliers</a></li>
            <!-- <li><a href="#">Sale</a></li> -->
            
            <!-- <li class="divider"></li>
            <li><a href="">Account</a></li> -->
          </ul>
        </li>
        <li id="TopAddSupplier" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Suppliers<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/Suppliers/index">Add Supplier</a></li>
            <li><a href="/Suppliers/edit">Edit Suppliers</a></li>
            <!-- <li><a href="#">Sale</a></li> -->
            
            <!-- <li class="divider"></li>
            <li><a href="">Account</a></li> -->
          </ul>
        </li>
      </ul>

      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><?php if(AuthComponent::user('name')==NUll){ ?><a href="/Settings/login">Login</a> <?php } else{?>
        <a href="/Settings/logout"><?php echo AuthComponent::user('name').' (logout)'?></a><?php } ?></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Settings <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Account</a></li>

          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
    </nav>


    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li id="Overview" class=""><a href="/Admins">Overview</a></li>
            <li id="Purchases"><a href="/Admins/purchases">Purchases</a></li>
           
          </ul>
          <ul class="nav nav-sidebar">
            <li id="liAddProducts"><a href="/Admins/Add">Add Products</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li id="liAddSupplier"><a href="/Suppliers">Add Suppliers</a></li>
            <li id="liEditSupplier"><a href="/Suppliers/edit">Manage Suppliers</a></li>
          </ul>
        </div>
<?php echo $this->fetch('content'); ?>
		
		<hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->



  </body>
</html>
<script type="text/javascript">
  $( "a" ).click(function() {
 
});
</script>
<link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/dashboard.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/doc.min.js"></script>
    <script src="/js/bug10.js"></script>
