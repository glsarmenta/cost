<div class = 'full'><div class="container">
    <div class="jumbotron">
        <h1>Search results</h1>
        <h2>Search words : <?php echo $words;?></h2>
        <h2><?php echo $this->Session->flash('warn') ?></h2>
    </div>

<?php foreach($data as $info=>$key){ ?>
    <div class="col-sm-4 col-lg-4 col-md-4">
        <div id ="itemDiv" class="thumbnail" onclick="location.href='/Tops/Clothing/<?php echo $key['SearchIndex']['id']; ?>';" style="cursor: pointer;">
            <img style ="display:block; height:200px; width:auto;" src=<?php echo '"/files/SearchIndexs4/'.$key['SearchIndex']['file_name'].'"';?>  alt="..." class="img-thumbnail">
            <h4 class="pull-right">
                <div class="label label-info"><?php echo $key['SearchIndex']['selling_price'];?></div>
            </h4>
            <div class="caption">
                <a href="/Tops/Clothing/<?php echo $key['SearchIndex']['id']; ?> "  data-toggle="tooltip" data-placement="bottom" title="<?php echo 
                str_replace('COST', '', $key['SearchIndex']['name']);?>"><h3 class="text-capitalize" style="white-space: nowrap; word-wrap: break-word;"><?php echo 
                substr(str_replace('COST', '', $key['SearchIndex']['name']),0,20).'...';?></h3></a>
            </div>
        </div>
    </div>
<?php }?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').on("click", "button[id=buy]", function (){
        var url = '/Tops/addCart/' + $(this).attr("data-product");
        $.ajax({
            type: "POST",
            url: url,
            success: function(data) {
                console.log(data);
                $("#checkoutnumber").empty();
                $("#checkoutnumber").append( data );
            }
        });
    });
});
</script>
<style type="text/css"></style>
