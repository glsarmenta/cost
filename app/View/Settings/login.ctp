
    <?php echo $this->Form->create('User',array('url/'=>$this->Html->url(array('controller'=>'/settings', 'action'=>'/login')))); ?>
    <div class="container">
    <div class ="row">

      
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
         <?php echo $this->Form->input('email',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Email','div'=>false,'label'=>false,'required'=>false,'error'=>false,'autofocus'=>true)); ?>
                    <?php 
                         echo $this->Form->error('User.email'); ?>
                         </br>
        <label for="inputPassword" class="sr-only">Password</label>
        <?php echo $this->Form->input('password',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter password','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                    <?php 
                         echo $this->Form->error('User.password'); ?>
                         <?php if ($logErr!=''){?><div class="error-message">
            <?php
          echo $logErr;
          ?>
        </div><?php }?>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button><?php echo $this->Form->end();?>
        <div>
          </br>
          <p><a class="btn btn-lg btn-primary btn-block" href="/Settings/registration" role="button">Create Account</a></p>
    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 
  
