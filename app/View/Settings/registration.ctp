
<div class="container">
    <div class="row">
        
            <?php echo $this->Form->create('User'); ?>
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
                <div class="form-group">
                    <label for="Username">Username</label>
                    <div class="input-group"><?php echo $this->Form->input('name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Username','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>                        
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.name'); ?>
                    
                </div>
                <div class="form-group">
                    <label for="first_name">Firstname</label>
                    <div class="input-group">
                        <?php echo $this->Form->input('first_name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Firstname','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.first_name'); ?>
                </div>
                <div class="form-group">
                    <label for="Lastname">Lastname</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('last_name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Lastname','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                     <?php 
                         echo $this->Form->error('User.last_name'); ?>
                </div>
                <div class="form-group">
                    <label for="Middlename">Middlename</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('middle_name',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Middlename','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                     <?php 
                         echo $this->Form->error('User.middle_name'); ?>
                </div>
                <div class="form-group">
                    <label for="Address">Address</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('address',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Address','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                     <?php 
                         echo $this->Form->error('User.address'); ?>
                </div>
                <div class="form-group">
                    <label for="Email">Email</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('email',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Email','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.email'); ?>
                </div>
                <div class="form-group">
                    <label for="Phonenumber">Phonenumber</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('phone_number',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Phonenumber','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.phone_number'); ?>
                </div>
                <div class="form-group">
                    <label for="Credit Card">Credit Card / Other Payment Method</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('credit_card',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Credit Card / Other Payment Method','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.credit_card'); ?>
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('password',
                    array('class'=>'form-control',
                    'placeholder'=>'Enter Password','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.password'); ?>
                </div>
                <div class="form-group">
                    <label for="Confirm Password">Confirm Password</label>
                    <div class="input-group">
                       <?php echo $this->Form->input('re_password',
                    array('class'=>'form-control','type'=>'password',
                    'placeholder'=>'Enter Confirm Password','div'=>false,'label'=>false,'required'=>false,'error'=>false)); ?>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                    <?php 
                         echo $this->Form->error('User.re_password'); ?>
                </div>
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            
        </form>
    </div>
</div>
