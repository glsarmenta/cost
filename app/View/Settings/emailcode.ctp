<div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h2>Email activation for COST customer</h2>
        <p>Cost functionality requires account activation, an activation key has been sent to the e-mail address you provided.</p>
        
        <p>
          <?php echo $this->Form->create('User',array('class' => 'form-inline')); ?>
            <div class="form-group">
              <label for="exampleInputName2">Fullname:</label>
              <input type="text" name="name"class="form-control" id="exampleInputName2" placeholder="Fullname">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Email</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail2" placeholder="Username@example.com">
            </div>
            <!-- <input type="button" value="click me to show message" onclick="setTimeout('window.alert(\'Hello!\')', 3000)" /> -->
           <button type="submit" class="btn btn-default">Confirm</button><?php echo $this->Form->end();?>
        </p> 

      </div>

    </div> <!-- /container -->

