-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2015 at 05:42 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cost`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_for_search_index`()
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE p_name VARCHAR(100);
    DECLARE p_description VARCHAR(2000);
    DECLARE p_view_status TINYINT;
    DECLARE p_file_name VARCHAR(255);
    DECLARE s_selling_price FLOAT(10,2);
    DECLARE idx TEXT;
    DECLARE p_deleted TINYINT;

    DECLARE cur CURSOR FOR 
            SELECT
                    p.name AS p_name
                  , p.description AS p_description
                  , p.file_name AS p_file_name
                  , p.view_status AS p_view_status
                  , p.deleted AS p_deleted
                  , s.price AS s_selling_price
              FROM products p
             INNER JOIN (
                SELECT
                    product_id
                  , price
                  FROM selling_prices
                 GROUP BY product_id
                 ORDER BY created DESC
                ) s
                ON s.product_id = p.id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO
                  p_name
                , p_description 
                , p_file_name
                , p_view_status
                , p_deleted
                , s_selling_price;

            SET idx =
                CONCAT_WS(' '
                , IFNULL(p_name, ' ')
                , IFNULL(p_description, ' ')
                );

            IF done THEN
                LEAVE ins_loop;
            END IF;

            INSERT INTO search_indices (
                  name
                , description
                , file_name
                , selling_price
                , view_status
                , idx
                , created
                , modified
                , deleted
            ) VALUES (
                  p_name
                , p_description
                , p_file_name
                , s_selling_price
                , p_view_status
                , idx
                , NOW()
                , NOW()
                , p_deleted
            );
        END LOOP;
    CLOSE cur;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emailcodes`
--

CREATE TABLE IF NOT EXISTS `emailcodes` (
`id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `middle_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `bank_account` varchar(100) CHARACTER SET utf8 NOT NULL,
  `credit_card` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `emailcode` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailcodes`
--

INSERT INTO `emailcodes` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `created`, `modified`, `deleted`, `deleted_date`, `emailcode`) VALUES
(14, 'zxc', '111111111', '<script>alert(''Hello''); </script>', '<script>alert(''Hello''); </script>', 'Legaspi', '<script>alert(''Hello''); </script>', 'g@yahoo.com', '1', '', '11111111', '2015-03-20 23:57:14', '2015-03-20 23:57:14', NULL, '2015-03-20 11:57:14', '4379e6d6eb9634f9bce8708b580d5e97'),
(15, 'DenmarkTan', 'december2191', 'Denmark Anthony', 'Tan', 'Salvador', 'Obando, Bulacan', 'denmark_tan2002@yahoo.com', '12345678', '', '12345678', '2015-03-21 08:06:24', '2015-03-21 08:06:24', NULL, '2015-03-21 08:06:24', '5bcf59eff3cff313a171132cab7f31b0'),
(16, 'DenmarkTan', 'december2191', 'Denmark Anthony', 'Tan', 'Salvador', 'Obando, Bulacan', 'denmark_tan2002@yahoo.com', '12345678', '', '12345678', '2015-03-21 08:12:49', '2015-03-21 08:12:49', NULL, '2015-03-21 08:12:49', '5bcf59eff3cff313a171132cab7f31b0');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
`id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `size`, `product_id`, `price_id`, `amount_of_order`, `amount_of_delivered`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 0, 1, 1, 1, 4, '2015-03-21 11:45:07', '2015-03-21 11:45:07', NULL, NULL),
(2, 0, 1, 1, 1, 15, '2015-03-21 12:10:51', '2015-03-21 12:10:51', NULL, NULL),
(3, 0, 1, 1, 1, 15, '2015-03-21 13:58:34', '2015-03-21 13:58:34', NULL, NULL),
(4, 1, 1, 1, 1, 15, '2015-03-21 14:21:16', '2015-03-21 14:21:16', NULL, NULL),
(5, 1, 1, 1, 1, 15, '2015-03-21 14:21:59', '2015-03-21 14:21:59', NULL, NULL),
(6, 1, 1, 1, 1, 15, '2015-03-21 14:22:48', '2015-03-21 14:22:48', NULL, NULL),
(7, 1, 1, 1, 1, 15, '2015-03-21 14:23:41', '2015-03-21 14:23:41', NULL, NULL),
(8, 1, 1, 1, 1, 15, '2015-03-21 14:24:12', '2015-03-21 14:24:12', NULL, NULL),
(9, 1, 1, 1, 1, 15, '2015-03-21 14:24:53', '2015-03-21 14:24:53', NULL, NULL),
(10, 0, 1, 1, 1, 15, '2015-03-21 14:40:43', '2015-03-21 14:40:43', NULL, NULL),
(11, 0, 1, 1, 1, 15, '2015-03-21 14:42:28', '2015-03-21 14:42:28', NULL, NULL),
(12, 0, 1, 1, 1, 15, '2015-03-21 14:43:11', '2015-03-21 14:43:11', NULL, NULL),
(13, 0, 1, 1, 1, 15, '2015-03-21 14:44:04', '2015-03-21 14:44:04', NULL, NULL),
(14, 0, 1, 1, 1, 15, '2015-03-21 14:48:43', '2015-03-21 14:48:43', NULL, NULL),
(15, 0, 1, 1, 1, 15, '2015-03-21 14:48:43', '2015-03-21 14:48:43', NULL, NULL),
(16, 0, 1, 1, 1, 15, '2015-03-21 14:48:43', '2015-03-21 14:48:43', NULL, NULL),
(17, 0, 1, 1, 1, 15, '2015-03-21 14:48:43', '2015-03-21 14:48:43', NULL, NULL),
(18, 0, 1, 1, 1, 15, '2015-03-21 14:48:43', '2015-03-21 14:48:43', NULL, NULL),
(19, 0, 1, 1, 1, 15, '2015-03-21 14:49:16', '2015-03-21 14:49:16', NULL, NULL),
(20, 0, 1, 1, 1, 15, '2015-03-21 14:49:16', '2015-03-21 14:49:16', NULL, NULL),
(21, 0, 1, 1, 1, 15, '2015-03-21 14:49:16', '2015-03-21 14:49:16', NULL, NULL),
(22, 0, 1, 1, 1, 15, '2015-03-21 14:49:16', '2015-03-21 14:49:16', NULL, NULL),
(23, 0, 1, 1, 2, 15, '2015-03-21 14:49:16', '2015-03-21 14:49:16', NULL, NULL),
(24, 1, 1, 1, 1, 15, '2015-03-21 14:50:49', '2015-03-21 14:50:49', NULL, NULL),
(25, 3, 1, 1, 1, 15, '2015-03-21 14:50:49', '2015-03-21 14:50:49', NULL, NULL),
(26, 3, 1, 1, 1, 15, '2015-03-21 14:50:49', '2015-03-21 14:50:49', NULL, NULL),
(27, 3, 1, 1, 1, 15, '2015-03-21 14:50:49', '2015-03-21 14:50:49', NULL, NULL),
(28, 2, 1, 1, 2, 15, '2015-03-21 14:50:49', '2015-03-21 14:50:49', NULL, NULL),
(29, 1, 1, 1, 1, 15, '2015-03-21 15:25:52', '2015-03-21 15:25:52', NULL, NULL),
(30, 3, 1, 1, 1, 15, '2015-03-21 15:25:52', '2015-03-21 15:25:52', NULL, NULL),
(31, 3, 1, 1, 1, 15, '2015-03-21 15:25:52', '2015-03-21 15:25:52', NULL, NULL),
(32, 3, 1, 1, 1, 15, '2015-03-21 15:25:52', '2015-03-21 15:25:52', NULL, NULL),
(33, 2, 1, 1, 2, 15, '2015-03-21 15:25:53', '2015-03-21 15:25:53', NULL, NULL),
(34, 1, 1, 1, 1, 15, '2015-03-21 15:27:11', '2015-03-21 15:27:11', NULL, NULL),
(35, 3, 1, 1, 1, 15, '2015-03-21 15:27:11', '2015-03-21 15:27:11', NULL, NULL),
(36, 3, 1, 1, 1, 15, '2015-03-21 15:27:11', '2015-03-21 15:27:11', NULL, NULL),
(37, 3, 1, 1, 1, 15, '2015-03-21 15:27:11', '2015-03-21 15:27:11', NULL, NULL),
(38, 2, 1, 1, 2, 15, '2015-03-21 15:27:11', '2015-03-21 15:27:11', NULL, NULL),
(39, 1, 1, 1, 1, 15, '2015-03-21 15:30:02', '2015-03-21 15:30:02', NULL, NULL),
(40, 1, 1, 1, 1, 15, '2015-03-21 15:32:56', '2015-03-21 15:32:56', NULL, NULL),
(41, 1, 1, 1, 1, 15, '2015-03-21 15:37:12', '2015-03-21 15:37:12', NULL, NULL),
(42, 1, 1, 1, 1, 15, '2015-03-21 15:40:13', '2015-03-21 15:40:13', NULL, NULL),
(43, 1, 1, 1, 1, 15, '2015-03-21 15:41:22', '2015-03-21 15:41:22', NULL, NULL),
(44, 3, 1, 1, 1, 15, '2015-03-21 15:41:22', '2015-03-21 15:41:22', NULL, NULL),
(45, 3, 1, 1, 1, 15, '2015-03-21 15:41:22', '2015-03-21 15:41:22', NULL, NULL),
(46, 3, 1, 1, 1, 15, '2015-03-21 15:41:22', '2015-03-21 15:41:22', NULL, NULL),
(47, 2, 1, 1, 1, 15, '2015-03-21 15:41:22', '2015-03-21 15:41:22', NULL, NULL),
(48, 1, 1, 1, 1, 15, '2015-03-21 15:41:24', '2015-03-21 15:41:24', NULL, NULL),
(49, 3, 1, 1, 1, 15, '2015-03-21 15:41:24', '2015-03-21 15:41:24', NULL, NULL),
(50, 3, 1, 1, 1, 15, '2015-03-21 15:41:24', '2015-03-21 15:41:24', NULL, NULL),
(51, 3, 1, 1, 1, 15, '2015-03-21 15:41:25', '2015-03-21 15:41:25', NULL, NULL),
(52, 2, 1, 1, 1, 15, '2015-03-21 15:41:25', '2015-03-21 15:41:25', NULL, NULL),
(53, 1, 1, 1, 1, 15, '2015-03-21 15:41:42', '2015-03-21 15:41:42', NULL, NULL),
(54, 3, 1, 1, 1, 15, '2015-03-21 15:41:42', '2015-03-21 15:41:42', NULL, NULL),
(55, 3, 1, 1, 1, 15, '2015-03-21 15:41:43', '2015-03-21 15:41:43', NULL, NULL),
(56, 3, 1, 1, 1, 15, '2015-03-21 15:41:43', '2015-03-21 15:41:43', NULL, NULL),
(57, 2, 1, 1, 1, 15, '2015-03-21 15:41:43', '2015-03-21 15:41:43', NULL, NULL),
(58, 1, 1, 1, 1, 15, '2015-03-21 15:42:46', '2015-03-21 15:42:46', NULL, NULL),
(59, 1, 1, 1, 1, 15, '2015-03-21 15:43:12', '2015-03-21 15:43:12', NULL, NULL),
(60, 1, 1, 1, 1, 15, '2015-03-21 15:43:36', '2015-03-21 15:43:36', NULL, NULL),
(61, 1, 1, 1, 1, 15, '2015-03-21 15:43:42', '2015-03-21 15:43:42', NULL, NULL),
(62, 1, 1, 1, 1, 15, '2015-03-21 15:44:43', '2015-03-21 15:44:43', NULL, NULL),
(63, 1, 1, 1, 1, 15, '2015-03-21 15:45:10', '2015-03-21 15:45:10', NULL, NULL),
(64, 1, 1, 1, 1, 15, '2015-03-21 15:45:32', '2015-03-21 15:45:32', NULL, NULL),
(65, 1, 1, 1, 1, 15, '2015-03-21 15:55:22', '2015-03-21 15:55:22', NULL, NULL),
(66, 1, 1, 1, 1, 15, '2015-03-21 15:56:21', '2015-03-21 15:56:21', NULL, NULL),
(67, 1, 1, 1, 1, 15, '2015-03-21 15:56:51', '2015-03-21 15:56:51', NULL, NULL),
(68, 1, 1, 1, 1, 15, '2015-03-21 15:58:21', '2015-03-21 15:58:21', NULL, NULL),
(69, 3, 1, 1, 1, 15, '2015-03-21 15:58:21', '2015-03-21 15:58:21', NULL, NULL),
(70, 3, 1, 1, 1, 15, '2015-03-21 15:58:21', '2015-03-21 15:58:21', NULL, NULL),
(71, 3, 1, 1, 1, 15, '2015-03-21 15:58:21', '2015-03-21 15:58:21', NULL, NULL),
(72, 2, 1, 1, 1, 15, '2015-03-21 15:58:21', '2015-03-21 15:58:21', NULL, NULL),
(73, 1, 1, 1, 1, 15, '2015-03-21 15:59:54', '2015-03-21 15:59:54', NULL, NULL),
(74, 3, 1, 1, 1, 15, '2015-03-21 15:59:54', '2015-03-21 15:59:54', NULL, NULL),
(75, 3, 1, 1, 1, 15, '2015-03-21 15:59:54', '2015-03-21 15:59:54', NULL, NULL),
(76, 3, 1, 1, 1, 15, '2015-03-21 15:59:54', '2015-03-21 15:59:54', NULL, NULL),
(77, 2, 1, 1, 1, 15, '2015-03-21 15:59:54', '2015-03-21 15:59:54', NULL, NULL),
(78, 1, 1, 1, 1, 15, '2015-03-21 16:01:45', '2015-03-21 16:01:45', NULL, NULL),
(79, 1, 1, 1, 1, 15, '2015-03-21 16:01:46', '2015-03-21 16:01:46', NULL, NULL),
(80, 1, 1, 1, 1, 15, '2015-03-21 16:05:08', '2015-03-21 16:05:08', NULL, NULL),
(81, 1, 1, 1, 1, 15, '2015-03-21 16:06:33', '2015-03-21 16:06:33', NULL, NULL),
(82, 1, 1, 1, 1, 15, '2015-03-21 16:07:26', '2015-03-21 16:07:26', NULL, NULL),
(83, 1, 1, 1, 1, 15, '2015-03-21 16:09:23', '2015-03-21 16:09:23', NULL, NULL),
(84, 3, 3, 3, 1, 15, '2015-03-24 16:31:41', '2015-03-24 16:31:41', NULL, NULL),
(85, 2, 16, 16, 1, 15, '2015-03-24 16:31:41', '2015-03-24 16:31:41', NULL, NULL),
(86, 3, 3, 3, 1, 15, '2015-03-24 16:31:43', '2015-03-24 16:31:43', NULL, NULL),
(87, 2, 16, 16, 1, 15, '2015-03-24 16:31:43', '2015-03-24 16:31:43', NULL, NULL),
(88, 3, 3, 3, 2, 15, '2015-03-24 16:31:50', '2015-03-24 16:31:50', NULL, NULL),
(89, 2, 16, 16, 1, 15, '2015-03-24 16:31:50', '2015-03-24 16:31:50', NULL, NULL),
(90, 3, 3, 3, 2, 15, '2015-03-24 16:31:56', '2015-03-24 16:31:56', NULL, NULL),
(91, 2, 16, 16, 1, 15, '2015-03-24 16:31:56', '2015-03-24 16:31:56', NULL, NULL),
(92, 3, 3, 3, 1, 15, '2015-03-24 16:32:02', '2015-03-24 16:32:02', NULL, NULL),
(93, 2, 16, 16, 1, 15, '2015-03-24 16:32:02', '2015-03-24 16:32:02', NULL, NULL),
(94, 3, 3, 3, 1, 15, '2015-03-24 16:32:06', '2015-03-24 16:32:06', NULL, NULL),
(95, 2, 16, 16, 1, 15, '2015-03-24 16:32:06', '2015-03-24 16:32:06', NULL, NULL),
(96, 3, 3, 3, 1, 15, '2015-03-24 16:32:06', '2015-03-24 16:32:06', NULL, NULL),
(97, 2, 16, 16, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(98, 3, 3, 3, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(99, 2, 16, 16, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(100, 3, 3, 3, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(101, 2, 16, 16, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(102, 3, 3, 3, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(103, 2, 16, 16, 1, 15, '2015-03-24 16:32:07', '2015-03-24 16:32:07', NULL, NULL),
(104, 3, 3, 3, 1, 15, '2015-03-24 16:32:08', '2015-03-24 16:32:08', NULL, NULL),
(105, 2, 16, 16, 1, 15, '2015-03-24 16:32:08', '2015-03-24 16:32:08', NULL, NULL),
(106, 3, 3, 3, 1, 15, '2015-03-24 16:32:08', '2015-03-24 16:32:08', NULL, NULL),
(107, 2, 16, 16, 1, 15, '2015-03-24 16:32:08', '2015-03-24 16:32:08', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
`id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount_of_payment` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE IF NOT EXISTS `payment_methods` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `procurements`
--

CREATE TABLE IF NOT EXISTS `procurements` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `product_categories_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `release_date` datetime DEFAULT NULL,
  `reservation_flg` tinyint(4) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_categories_id`, `name`, `description`, `supplier_id`, `file_name`, `release_date`, `reservation_flg`, `view_status`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(2, 5, 'COST Scoop Back Midi Dress', 'There will be smiles all round when you wear our favourite knit midi dress! Featuring short sleeves, and a flattering scoop back.', 9, '1427062628_COST Scoop Back Midi Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 06:17:08', '2015-03-27 23:49:35', NULL, NULL),
(3, 5, 'COST Women Cotton Cashmere Striped Dress', 'This stylish dress is made from cotton material blended with luxurious cashmere for a soft, elegant texture. The simple design and casual striped pattern let you layer it with a variety of items to create new looks.', 9, '1427063292_COST WOMEN COTTON CASHMERE STRIPED DRESS.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 06:28:12', '0000-00-00 00:00:00', NULL, NULL),
(4, 5, 'COST Panelled Sheath Dress', 'Electric Blue/Black Panelled Sheath Dress features clashing abstract prints following a monochromatic theme with gold tone zipper fastening to the back', 9, '1427067131_COST Panelled Sheath Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:32:11', '0000-00-00 00:00:00', NULL, NULL),
(5, 5, 'COST Double Pleat Fit & Flare Dress', 'Double Pleat Fit & Flare Dress features a simple and feminine design with a subtle all over pattern.', 9, '1427067257_COST Double Pleat Fit & Flare Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:34:17', '0000-00-00 00:00:00', NULL, NULL),
(6, 5, 'COST Lace Elastic Back Bodycon', 'From the pretty lace sleeves to the edgy elastic bands at the back, there is so much to love about Bodycon Dress from Material Girl. With the perfect balance of dainty and daring, the form-fitting silhouette will define your curves and flatter the best side of you. ', 9, '1427067555_COST Lace Elastic Back Bodycon.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:39:15', '0000-00-00 00:00:00', NULL, NULL),
(7, 5, 'COST Collared Shirt Dress', 'Collared Shirt Dress by Something Borrowed features a sleeveless shirt dress with colour-blocking sides, chic and stylish, perfect for any occasion.\r\n', 9, '1427067730_COST Collared Shirt Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:42:10', '0000-00-00 00:00:00', NULL, NULL),
(8, 5, 'COST Chiara Sleeveless Long Dress', 'The Chiara Sleeveless Long Dress from Spring Fling will round off casual and chic ensembles. Wear to work, parties or informal receptions.', 9, '1427067864_COST Chiara Sleeveless Long Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:44:24', '0000-00-00 00:00:00', NULL, NULL),
(9, 5, 'COST Taylor Mock Wrap Dress', 'Update your casual wardrobe with chic printed staples like the Taylor Mock Wrap Dress. It''s a modish piece to emulate an effortlessly posh look. Always be stylish with trendy apparels from Chloe Edit. ', 9, '1427068090_COST Taylor Mock Wrap Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:48:10', '0000-00-00 00:00:00', NULL, NULL),
(10, 5, 'COST Floral Print Dress', 'Relax in chic with this gorgeous number by Mango, featuring soft fabric with ethereal floral prints, and a fun drawstring tassels along the front.', 9, '1427068181_COST Floral Print Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:49:41', '0000-00-00 00:00:00', NULL, NULL),
(11, 5, 'COST Rib Bodycon Dress', 'This Dress features a figure-forming silhouette with a ribbed design. Slip on this dress for a dose of minimalist chic.\r\n', 9, '1427068485_COST Rib Bodycon Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:54:45', '0000-00-00 00:00:00', NULL, NULL),
(12, 5, 'COST Tasseled Abstract Print Cami Dress', 'Elevate your look with this gorgeous piece, featuring thick horizontal stripes and a plunging V-neckline and backline.', 9, '1427068722_COST Tasseled Abstract Print Cami Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 07:58:42', '0000-00-00 00:00:00', NULL, NULL),
(13, 5, 'COST Dave Dress', 'This Cora Dress is classy and feminine with a hint of sexy. It features strategic solid-tone panels to emphasize your natural figure.\r\n', 9, '1427069059_COST Dave Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 08:04:19', '0000-00-00 00:00:00', NULL, NULL),
(14, 5, 'COST Spring Fling', 'The Kanani Dress from Spring Fling makes casual dressing more fun and feminine. The round neckline and loose fit makes this piece great for urban chic events.', 9, '1427069293_COST Spring Fling.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 08:08:13', '0000-00-00 00:00:00', NULL, NULL),
(15, 5, 'COST Draped Tie-Back Midi Dress', 'Airy and drapey in lightweight crepe, this sleeveless midi dress is meticulously seamed and finished with a self-tie sash at the back for a sleek, structured silhouette. Dress this elegant number up for the office with a blazer, or dress it down for days at the park with sandals.', 9, '1427099160_COST Draped Tie-Back Midi Dress.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:26:00', '0000-00-00 00:00:00', NULL, NULL),
(16, 1, 'COST Distressed Linen Tee', 'An edgy remake of an everyday favorite, this tee is crafted from lightweight linen with allover distressing for a coolly nonchalant look. Offset its threadbare design with crisp, classic denim.', 9, '1427099556_COST Distressed Linen Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:32:36', '0000-00-00 00:00:00', NULL, NULL),
(17, 1, 'COST Speckle-Textured Pocket Tee', 'This classic short-sleeved pocket tee gets an eye-catching upgrade courtesy of subtly textured multi-colored speckles (just think of them as a new angle on a traditional polka dot).Speckle-Textured Pocket Tee', 9, '1427099934_COST Speckle Textured Pocket Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:38:54', '0000-00-00 00:00:00', NULL, NULL),
(18, 1, 'COST Heathered Henley Shirt', 'Rendered in a soft heathered knit, this short-sleeved henley is a seasonless essential. It features a two-button placket, a ribbed crew neck, and a relaxed fit. Wear it as an underlayer to coats and sweaters when it''s cold out, or rock it with chino shorts when temps start to rise.', 9, '1427100548_COST Dotted Elephant Print Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:49:08', '0000-00-00 00:00:00', NULL, NULL),
(19, 1, 'COST Chain Print Mesh Tee', 'Crafted from sleek athletic mesh and printed with oversized links of curb chain, this short-sleeved tee is anything but your standard whites.', 9, '1427100689_COST Chain Print Mesh Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:51:29', '0000-00-00 00:00:00', NULL, NULL),
(20, 1, 'COST Nautical Palm Print Tee', 'Printed with a silhouetted mix of willowy palm trees and sleek sailboats, this short-sleeved tee is a great way to add a seaworthy touch to your lineup of basics (a worthwhile pursuit, even if you''re landlocked).', 9, '1427100924_COST Nautical Palm Print Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:55:24', '0000-00-00 00:00:00', NULL, NULL),
(21, 1, 'COST Graphic Baseball Shirt', 'Covered in a mix of eye-catching prints (including an abstract design, stars, and a grid), this L.A.T.H.C.â„¢ baseball shirt - fittingly outfitted with a jersey-style strikethrough "79" graphic on its back - has street and standout style on lock. Play up its sporty look by pairing it with joggers, or let it add edge to an everyday staple like denim.', 9, '1427101087_COST Graphic Baseball Shirt.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 16:58:07', '0000-00-00 00:00:00', NULL, NULL),
(22, 1, 'COST Reverse Abstract Print Sweatshirt', 'We turned a traditional French terry sweatshirt on its head (literally), using the loop-knit side usually used on the interior to create a cool textural element on this laid-back long-sleeved layer''s exterior instead. Bonus: a mix of dynamic, sketchbook-style abstract prints is a striking departure from your everyday stripes and solids.', 9, '1427117168_COST Reverse Abstract Print Sweatshirt.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 21:26:08', '0000-00-00 00:00:00', NULL, NULL),
(23, 1, 'COST Baseball Collar Oxford Shirt', 'COST take on the classic long-sleeved cotton oxford is a little less buttoned-up, a little more "batter up" thanks to its cool striped baseball collar. ', 9, '1427117354_COST Baseball Collar Oxford Shirt.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 21:29:14', '0000-00-00 00:00:00', NULL, NULL),
(24, 1, 'COST Basquiat Graphic Tee', 'For the art aficionado, this Basquiat graphic baseball tee is sure to please. It''s crafted from a super-soft and comfy cotton and displays a print of one of the late artist''s most famous drawings of the legendary boxer Jack Johnson. Complete with a crew neckline and contrast raglan sleeves, this tee is a total must-have for the Basquiat lover.', 9, '1427120464_COST Basquiat Graphic Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-23 22:21:04', '0000-00-00 00:00:00', NULL, NULL),
(25, 1, 'COST Classic Heathered Tee', 'Rendered in a super-soft heathered knit with short sleeves and a ribbed crew neckline, this tee is a basic at its best. Its lightweight feel ensures all-day comfort, and its classic simplicity means it''ll go with everything you own - it''s essentially the key to getting out the door in under ten.', 9, '1427172292_COST Classic Heathered Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:44:52', '0000-00-00 00:00:00', NULL, NULL),
(26, 1, 'COST Slim-Striped Tee', 'Cool slim stripes (actually, stripes made of stripes) are what separate this crew-neck tee from the others. Great solo or under a jacket for a pop of pattern, it''s an easy, casual piece for the well-heeled guy.', 9, '1427172465_COST Slim-Striped Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:47:45', '0000-00-00 00:00:00', NULL, NULL),
(27, 1, 'COST Southwestern-Patterned Tee', 'This short-sleeved tee has ruggedly laid-back style in spades thanks to its Southwestern-inspired pattern. It looks just as great worn alone as it does under a denim shirt or moto jacket (translation: this tee is pretty much for all seasons).', 9, '1427172640_COST Southwestern-Patterned Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:50:40', '0000-00-00 00:00:00', NULL, NULL),
(28, 1, 'COST Speckled Raw Edge Tee', 'Subtly marled for a cool speckled appearance, this short-sleeved tee has raw-cut edges that lend it the feel of a well-worn favorite (minus the whole breaking-it-in bit).', 9, '1427172775_COST Speckled Raw Edge Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:52:55', '0000-00-00 00:00:00', NULL, NULL),
(29, 1, 'COST Mesh-Paneled Colorblock Tee', 'We added panels of sporty athletic mesh and stripes of graphic colorblocking to this laid-back short-sleeved tee.', 9, '1427172872_COST Mesh-Paneled Colorblock Tee.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:54:32', '0000-00-00 00:00:00', NULL, NULL),
(30, 1, 'COST Striped Round-Hem', 'A mix of solid panels and high-impact stripes give this short-sleeved COST its sporty feel. A rounded hem and a just-right hint of slouch provide its easy air of cool.', 9, '1427173182_COST Striped Round-Hem.jpg', '0000-00-00 00:00:00', NULL, NULL, '2015-03-24 12:59:42', '0000-00-00 00:00:00', NULL, NULL);

--
-- Triggers `products`
--
DELIMITER //
CREATE TRIGGER `t_insert_product` AFTER INSERT ON `products`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    INSERT INTO search_indices (
          id
        , name
        , description
        ,filename
        
        
        , idx
        , created
        , modified
        , deleted
    ) VALUES (
          NEW.id
        , NEW.name
        , NEW.description
        
        , NEW.file_name
        
        , idx
        , NOW()
        , NOW()
        , NEW.deleted
    );
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `t_update_product` AFTER UPDATE ON `products`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    UPDATE search_indices SET
          name = NEW.name
        , description = NEW.description
        , filename = NEW.file_name
        
        
        , idx = idx
        , modified = NOW()
        , deleted = NEW.deleted
     WHERE id = NEW.id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
`id` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `gender`, `type`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 'T-Shirts', '2015-03-20 22:19:47', '2015-03-20 22:19:47', NULL, '2015-03-20 22:19:47'),
(5, 0, 'Dress', '2015-03-20 22:20:26', '2015-03-20 22:20:26', NULL, '2015-03-20 22:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `sort_order` tinyint(4) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `file_name`, `sort_order`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, '1426902710_COST Hooded Pinstripe Dress.jpg', 0, '2015-03-21 09:51:51', '0000-00-00 00:00:00', NULL, NULL),
(2, 2, '1427062628_COST Scoop Back Midi Dress.jpg', 0, '2015-03-23 06:17:09', '0000-00-00 00:00:00', NULL, NULL),
(3, 3, '1427063292_COST WOMEN COTTON CASHMERE STRIPED DRESS.jpg', 0, '2015-03-23 06:28:13', '0000-00-00 00:00:00', NULL, NULL),
(4, 4, '1427067131_COST Panelled Sheath Dress.jpg', 0, '2015-03-23 07:32:11', '0000-00-00 00:00:00', NULL, NULL),
(5, 5, '1427067257_COST Double Pleat Fit & Flare Dress.jpg', 0, '2015-03-23 07:34:17', '0000-00-00 00:00:00', NULL, NULL),
(6, 6, '1427067555_COST Lace Elastic Back Bodycon.jpg', 0, '2015-03-23 07:39:15', '0000-00-00 00:00:00', NULL, NULL),
(7, 7, '1427067730_COST Collared Shirt Dress.jpg', 0, '2015-03-23 07:42:10', '0000-00-00 00:00:00', NULL, NULL),
(8, 8, '1427067864_COST Chiara Sleeveless Long Dress.jpg', 0, '2015-03-23 07:44:25', '0000-00-00 00:00:00', NULL, NULL),
(9, 9, '1427068090_COST Taylor Mock Wrap Dress.jpg', 0, '2015-03-23 07:48:10', '0000-00-00 00:00:00', NULL, NULL),
(10, 10, '1427068181_COST Floral Print Dress.jpg', 0, '2015-03-23 07:49:41', '0000-00-00 00:00:00', NULL, NULL),
(11, 11, '1427068485_COST Rib Bodycon Dress.jpg', 0, '2015-03-23 07:54:46', '0000-00-00 00:00:00', NULL, NULL),
(12, 12, '1427068722_COST Tasseled Abstract Print Cami Dress.jpg', 0, '2015-03-23 07:58:42', '0000-00-00 00:00:00', NULL, NULL),
(13, 13, '1427069059_COST Dave Dress.jpg', 0, '2015-03-23 08:04:19', '0000-00-00 00:00:00', NULL, NULL),
(14, 14, '1427069293_COST Spring Fling.jpg', 0, '2015-03-23 08:08:14', '0000-00-00 00:00:00', NULL, NULL),
(15, 15, '1427099160_COST Draped Tie-Back Midi Dress.jpg', 0, '2015-03-23 16:26:01', '0000-00-00 00:00:00', NULL, NULL),
(16, 16, '1427099556_COST Distressed Linen Tee.jpg', 0, '2015-03-23 16:32:36', '0000-00-00 00:00:00', NULL, NULL),
(17, 17, '1427099934_COST Speckle Textured Pocket Tee.jpg', 0, '2015-03-23 16:38:54', '0000-00-00 00:00:00', NULL, NULL),
(18, 18, '1427100548_COST Dotted Elephant Print Tee.jpg', 0, '2015-03-23 16:49:09', '0000-00-00 00:00:00', NULL, NULL),
(19, 19, '1427100689_COST Chain Print Mesh Tee.jpg', 0, '2015-03-23 16:51:29', '0000-00-00 00:00:00', NULL, NULL),
(20, 20, '1427100924_COST Nautical Palm Print Tee.jpg', 0, '2015-03-23 16:55:24', '0000-00-00 00:00:00', NULL, NULL),
(21, 21, '1427101087_COST Graphic Baseball Shirt.jpg', 0, '2015-03-23 16:58:07', '0000-00-00 00:00:00', NULL, NULL),
(22, 22, '1427117168_COST Reverse Abstract Print Sweatshirt.jpg', 0, '2015-03-23 21:26:08', '0000-00-00 00:00:00', NULL, NULL),
(23, 23, '1427117354_COST Baseball Collar Oxford Shirt.jpg', 0, '2015-03-23 21:29:14', '0000-00-00 00:00:00', NULL, NULL),
(24, 24, '1427120464_COST Basquiat Graphic Tee.jpg', 0, '2015-03-23 22:21:04', '0000-00-00 00:00:00', NULL, NULL),
(25, 25, '1427172292_COST Classic Heathered Tee.jpg', 0, '2015-03-24 12:44:52', '0000-00-00 00:00:00', NULL, NULL),
(26, 26, '1427172465_COST Slim-Striped Tee.jpg', 0, '2015-03-24 12:47:46', '0000-00-00 00:00:00', NULL, NULL),
(27, 27, '1427172640_COST Southwestern-Patterned Tee.jpg', 0, '2015-03-24 12:50:40', '0000-00-00 00:00:00', NULL, NULL),
(28, 28, '1427172775_COST Speckled Raw Edge Tee.jpg', 0, '2015-03-24 12:52:55', '0000-00-00 00:00:00', NULL, NULL),
(29, 29, '1427172872_COST Mesh-Paneled Colorblock Tee.jpg', 0, '2015-03-24 12:54:33', '0000-00-00 00:00:00', NULL, NULL),
(30, 30, '1427173182_COST Striped Round-Hem.jpg', 0, '2015-03-24 12:59:42', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE IF NOT EXISTS `recommendations` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `search_indices`
--

CREATE TABLE IF NOT EXISTS `search_indices` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT NULL,
  `idx` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `search_indices`
--

INSERT INTO `search_indices` (`id`, `name`, `description`, `view_status`, `idx`, `created`, `modified`, `deleted`, `filename`) VALUES
(1, 'COST Hooded Pinstripe Dress', 'Take your comfort game to the next level with this sleeveless dress. It''s crafted from the same comfy knit as your favorite sweatshirt with an allover pinstripe pattern. Throw on the drawstring hood and slip your hands into the front kangaroo pocket for luxe lounging.', NULL, 'COST Hooded Pinstripe Dress Take your comfort game to the next level with this sleeveless dress. It''s crafted from the same comfy knit as your favorite sweatshirt with an allover pinstripe pattern. Throw on the drawstring hood and slip your hands into the front kangaroo pocket for luxe lounging.', '2015-03-21 09:51:50', '2015-03-27 23:47:47', NULL, '1426902710_COST Hooded Pinstripe Dress.jpg'),
(2, 'COST Scoop Back Midi Dress', 'There will be smiles all round when you wear our favourite knit midi dress! Featuring short sleeves, and a flattering scoop back.', NULL, 'COST Scoop Back Midi Dress There will be smiles all round when you wear our favourite knit midi dress! Featuring short sleeves, and a flattering scoop back.', '2015-03-23 06:17:08', '2015-03-27 23:49:35', NULL, '1427062628_COST Scoop Back Midi Dress.jpg'),
(3, 'COST Women Cotton Cashmere Striped Dress', 'This stylish dress is made from cotton material blended with luxurious cashmere for a soft, elegant texture. The simple design and casual striped pattern let you layer it with a variety of items to create new looks.', NULL, 'COST Women Cotton Cashmere Striped Dress This stylish dress is made from cotton material blended with luxurious cashmere for a soft, elegant texture. The simple design and casual striped pattern let you layer it with a variety of items to create new looks.', '2015-03-23 06:28:12', '2015-03-23 06:28:12', NULL, '1427063292_COST WOMEN COTTON CASHMERE STRIPED DRESS.jpg'),
(4, 'COST Panelled Sheath Dress', 'Electric Blue/Black Panelled Sheath Dress features clashing abstract prints following a monochromatic theme with gold tone zipper fastening to the back', NULL, 'COST Panelled Sheath Dress Electric Blue/Black Panelled Sheath Dress features clashing abstract prints following a monochromatic theme with gold tone zipper fastening to the back', '2015-03-23 07:32:11', '2015-03-23 07:35:40', NULL, '1427067131_COST Panelled Sheath Dress.jpg'),
(5, 'COST Double Pleat Fit & Flare Dress', 'Double Pleat Fit & Flare Dress features a simple and feminine design with a subtle all over pattern.', NULL, 'COST Double Pleat Fit & Flare Dress Double Pleat Fit & Flare Dress features a simple and feminine design with a subtle all over pattern.', '2015-03-23 07:34:17', '2015-03-23 07:34:17', NULL, '1427067257_COST Double Pleat Fit & Flare Dress.jpg'),
(6, 'COST Lace Elastic Back Bodycon', 'From the pretty lace sleeves to the edgy elastic bands at the back, there is so much to love about Bodycon Dress from Material Girl. With the perfect balance of dainty and daring, the form-fitting silhouette will define your curves and flatter the best side of you. ', NULL, 'COST Lace Elastic Back Bodycon From the pretty lace sleeves to the edgy elastic bands at the back, there is so much to love about Bodycon Dress from Material Girl. With the perfect balance of dainty and daring, the form-fitting silhouette will define your curves and flatter the best side of you. ', '2015-03-23 07:39:15', '2015-03-23 07:51:31', NULL, '1427067555_COST Lace Elastic Back Bodycon.jpg'),
(7, 'COST Collared Shirt Dress', 'Collared Shirt Dress by Something Borrowed features a sleeveless shirt dress with colour-blocking sides, chic and stylish, perfect for any occasion.\r\n', NULL, 'COST Collared Shirt Dress Collared Shirt Dress by Something Borrowed features a sleeveless shirt dress with colour-blocking sides, chic and stylish, perfect for any occasion.\r\n', '2015-03-23 07:42:10', '2015-03-23 07:42:10', NULL, '1427067730_COST Collared Shirt Dress.jpg'),
(8, 'COST Chiara Sleeveless Long Dress', 'The Chiara Sleeveless Long Dress from Spring Fling will round off casual and chic ensembles. Wear to work, parties or informal receptions.', NULL, 'COST Chiara Sleeveless Long Dress The Chiara Sleeveless Long Dress from Spring Fling will round off casual and chic ensembles. Wear to work, parties or informal receptions.', '2015-03-23 07:44:24', '2015-03-23 07:44:24', NULL, '1427067864_COST Chiara Sleeveless Long Dress.jpg'),
(9, 'COST Taylor Mock Wrap Dress', 'Update your casual wardrobe with chic printed staples like the Taylor Mock Wrap Dress. It''s a modish piece to emulate an effortlessly posh look. Always be stylish with trendy apparels from Chloe Edit. ', NULL, 'COST Taylor Mock Wrap Dress Update your casual wardrobe with chic printed staples like the Taylor Mock Wrap Dress. It''s a modish piece to emulate an effortlessly posh look. Always be stylish with trendy apparels from Chloe Edit. ', '2015-03-23 07:48:10', '2015-03-23 07:48:10', NULL, '1427068090_COST Taylor Mock Wrap Dress.jpg'),
(10, 'COST Floral Print Dress', 'Relax in chic with this gorgeous number by Mango, featuring soft fabric with ethereal floral prints, and a fun drawstring tassels along the front.', NULL, 'COST Floral Print Dress Relax in chic with this gorgeous number by Mango, featuring soft fabric with ethereal floral prints, and a fun drawstring tassels along the front.', '2015-03-23 07:49:41', '2015-03-23 07:49:41', NULL, '1427068181_COST Floral Print Dress.jpg'),
(11, 'COST Rib Bodycon Dress', 'This Dress features a figure-forming silhouette with a ribbed design. Slip on this dress for a dose of minimalist chic.\r\n', NULL, 'COST Rib Bodycon Dress This Dress features a figure-forming silhouette with a ribbed design. Slip on this dress for a dose of minimalist chic.\r\n', '2015-03-23 07:54:45', '2015-03-23 07:54:45', NULL, '1427068485_COST Rib Bodycon Dress.jpg'),
(12, 'COST Tasseled Abstract Print Cami Dress', 'Elevate your look with this gorgeous piece, featuring thick horizontal stripes and a plunging V-neckline and backline.', NULL, 'COST Tasseled Abstract Print Cami Dress Elevate your look with this gorgeous piece, featuring thick horizontal stripes and a plunging V-neckline and backline.', '2015-03-23 07:58:42', '2015-03-23 13:22:48', NULL, '1427068722_COST Tasseled Abstract Print Cami Dress.jpg'),
(13, 'COST Dave Dress', 'This Cora Dress is classy and feminine with a hint of sexy. It features strategic solid-tone panels to emphasize your natural figure.\r\n', NULL, 'COST Dave Dress This Cora Dress is classy and feminine with a hint of sexy. It features strategic solid-tone panels to emphasize your natural figure.\r\n', '2015-03-23 08:04:19', '2015-03-23 13:22:53', NULL, '1427069059_COST Dave Dress.jpg'),
(14, 'COST Spring Fling', 'The Kanani Dress from Spring Fling makes casual dressing more fun and feminine. The round neckline and loose fit makes this piece great for urban chic events.', NULL, 'COST Spring Fling The Kanani Dress from Spring Fling makes casual dressing more fun and feminine. The round neckline and loose fit makes this piece great for urban chic events.', '2015-03-23 08:08:13', '2015-03-23 08:08:13', NULL, '1427069293_COST Spring Fling.jpg'),
(15, 'COST Draped Tie-Back Midi Dress', 'Airy and drapey in lightweight crepe, this sleeveless midi dress is meticulously seamed and finished with a self-tie sash at the back for a sleek, structured silhouette. Dress this elegant number up for the office with a blazer, or dress it down for days at the park with sandals.', NULL, 'COST Draped Tie-Back Midi Dress Airy and drapey in lightweight crepe, this sleeveless midi dress is meticulously seamed and finished with a self-tie sash at the back for a sleek, structured silhouette. Dress this elegant number up for the office with a blazer, or dress it down for days at the park with sandals.', '2015-03-23 16:26:00', '2015-03-23 16:26:00', NULL, '1427099160_COST Draped Tie-Back Midi Dress.jpg'),
(16, 'COST Distressed Linen Tee', 'An edgy remake of an everyday favorite, this tee is crafted from lightweight linen with allover distressing for a coolly nonchalant look. Offset its threadbare design with crisp, classic denim.', NULL, 'COST Distressed Linen Tee An edgy remake of an everyday favorite, this tee is crafted from lightweight linen with allover distressing for a coolly nonchalant look. Offset its threadbare design with crisp, classic denim.', '2015-03-23 16:32:36', '2015-03-23 16:32:36', NULL, '1427099556_COST Distressed Linen Tee.jpg'),
(17, 'COST Speckle-Textured Pocket Tee', 'This classic short-sleeved pocket tee gets an eye-catching upgrade courtesy of subtly textured multi-colored speckles (just think of them as a new angle on a traditional polka dot).Speckle-Textured Pocket Tee', NULL, 'COST Speckle-Textured Pocket Tee This classic short-sleeved pocket tee gets an eye-catching upgrade courtesy of subtly textured multi-colored speckles (just think of them as a new angle on a traditional polka dot).Speckle-Textured Pocket Tee', '2015-03-23 16:38:54', '2015-03-23 16:38:54', NULL, '1427099934_COST Speckle Textured Pocket Tee.jpg'),
(18, 'COST Heathered Henley Shirt', 'Rendered in a soft heathered knit, this short-sleeved henley is a seasonless essential. It features a two-button placket, a ribbed crew neck, and a relaxed fit. Wear it as an underlayer to coats and sweaters when it''s cold out, or rock it with chino shorts when temps start to rise.', NULL, 'COST Heathered Henley Shirt Rendered in a soft heathered knit, this short-sleeved henley is a seasonless essential. It features a two-button placket, a ribbed crew neck, and a relaxed fit. Wear it as an underlayer to coats and sweaters when it''s cold out, or rock it with chino shorts when temps start to rise.', '2015-03-23 16:49:08', '2015-03-23 16:49:08', NULL, '1427100548_COST Dotted Elephant Print Tee.jpg'),
(19, 'COST Chain Print Mesh Tee', 'Crafted from sleek athletic mesh and printed with oversized links of curb chain, this short-sleeved tee is anything but your standard whites.', NULL, 'COST Chain Print Mesh Tee Crafted from sleek athletic mesh and printed with oversized links of curb chain, this short-sleeved tee is anything but your standard whites.', '2015-03-23 16:51:29', '2015-03-23 16:51:29', NULL, '1427100689_COST Chain Print Mesh Tee.jpg'),
(20, 'COST Nautical Palm Print Tee', 'Printed with a silhouetted mix of willowy palm trees and sleek sailboats, this short-sleeved tee is a great way to add a seaworthy touch to your lineup of basics (a worthwhile pursuit, even if you''re landlocked).', NULL, 'COST Nautical Palm Print Tee Printed with a silhouetted mix of willowy palm trees and sleek sailboats, this short-sleeved tee is a great way to add a seaworthy touch to your lineup of basics (a worthwhile pursuit, even if you''re landlocked).', '2015-03-23 16:55:24', '2015-03-23 16:55:24', NULL, '1427100924_COST Nautical Palm Print Tee.jpg'),
(21, 'COST Graphic Baseball Shirt', 'Covered in a mix of eye-catching prints (including an abstract design, stars, and a grid), this L.A.T.H.C.â„¢ baseball shirt - fittingly outfitted with a jersey-style strikethrough "79" graphic on its back - has street and standout style on lock. Play up its sporty look by pairing it with joggers, or let it add edge to an everyday staple like denim.', NULL, 'COST Graphic Baseball Shirt Covered in a mix of eye-catching prints (including an abstract design, stars, and a grid), this L.A.T.H.C.â„¢ baseball shirt - fittingly outfitted with a jersey-style strikethrough "79" graphic on its back - has street and standout style on lock. Play up its sporty look by pairing it with joggers, or let it add edge to an everyday staple like denim.', '2015-03-23 16:58:07', '2015-03-23 16:58:07', NULL, '1427101087_COST Graphic Baseball Shirt.jpg'),
(22, 'COST Reverse Abstract Print Sweatshirt', 'We turned a traditional French terry sweatshirt on its head (literally), using the loop-knit side usually used on the interior to create a cool textural element on this laid-back long-sleeved layer''s exterior instead. Bonus: a mix of dynamic, sketchbook-style abstract prints is a striking departure from your everyday stripes and solids.', NULL, 'COST Reverse Abstract Print Sweatshirt We turned a traditional French terry sweatshirt on its head (literally), using the loop-knit side usually used on the interior to create a cool textural element on this laid-back long-sleeved layer''s exterior instead. Bonus: a mix of dynamic, sketchbook-style abstract prints is a striking departure from your everyday stripes and solids.', '2015-03-23 21:26:08', '2015-03-23 21:26:08', NULL, '1427117168_COST Reverse Abstract Print Sweatshirt.jpg'),
(23, 'COST Baseball Collar Oxford Shirt', 'COST take on the classic long-sleeved cotton oxford is a little less buttoned-up, a little more "batter up" thanks to its cool striped baseball collar. ', NULL, 'COST Baseball Collar Oxford Shirt COST take on the classic long-sleeved cotton oxford is a little less buttoned-up, a little more "batter up" thanks to its cool striped baseball collar. ', '2015-03-23 21:29:14', '2015-03-23 21:29:14', NULL, '1427117354_COST Baseball Collar Oxford Shirt.jpg'),
(24, 'COST Basquiat Graphic Tee', 'For the art aficionado, this Basquiat graphic baseball tee is sure to please. It''s crafted from a super-soft and comfy cotton and displays a print of one of the late artist''s most famous drawings of the legendary boxer Jack Johnson. Complete with a crew neckline and contrast raglan sleeves, this tee is a total must-have for the Basquiat lover.', NULL, 'COST Basquiat Graphic Tee For the art aficionado, this Basquiat graphic baseball tee is sure to please. It''s crafted from a super-soft and comfy cotton and displays a print of one of the late artist''s most famous drawings of the legendary boxer Jack Johnson. Complete with a crew neckline and contrast raglan sleeves, this tee is a total must-have for the Basquiat lover.', '2015-03-23 22:21:04', '2015-03-23 22:21:04', NULL, '1427120464_COST Basquiat Graphic Tee.jpg'),
(25, 'COST Classic Heathered Tee', 'Rendered in a super-soft heathered knit with short sleeves and a ribbed crew neckline, this tee is a basic at its best. Its lightweight feel ensures all-day comfort, and its classic simplicity means it''ll go with everything you own - it''s essentially the key to getting out the door in under ten.', NULL, 'COST Classic Heathered Tee Rendered in a super-soft heathered knit with short sleeves and a ribbed crew neckline, this tee is a basic at its best. Its lightweight feel ensures all-day comfort, and its classic simplicity means it''ll go with everything you own - it''s essentially the key to getting out the door in under ten.', '2015-03-24 12:44:52', '2015-03-24 12:44:52', NULL, '1427172292_COST Classic Heathered Tee.jpg'),
(26, 'COST Slim-Striped Tee', 'Cool slim stripes (actually, stripes made of stripes) are what separate this crew-neck tee from the others. Great solo or under a jacket for a pop of pattern, it''s an easy, casual piece for the well-heeled guy.', NULL, 'COST Slim-Striped Tee Cool slim stripes (actually, stripes made of stripes) are what separate this crew-neck tee from the others. Great solo or under a jacket for a pop of pattern, it''s an easy, casual piece for the well-heeled guy.', '2015-03-24 12:47:45', '2015-03-24 12:47:45', NULL, '1427172465_COST Slim-Striped Tee.jpg'),
(27, 'COST Southwestern-Patterned Tee', 'This short-sleeved tee has ruggedly laid-back style in spades thanks to its Southwestern-inspired pattern. It looks just as great worn alone as it does under a denim shirt or moto jacket (translation: this tee is pretty much for all seasons).', NULL, 'COST Southwestern-Patterned Tee This short-sleeved tee has ruggedly laid-back style in spades thanks to its Southwestern-inspired pattern. It looks just as great worn alone as it does under a denim shirt or moto jacket (translation: this tee is pretty much for all seasons).', '2015-03-24 12:50:40', '2015-03-24 12:50:40', NULL, '1427172640_COST Southwestern-Patterned Tee.jpg'),
(28, 'COST Speckled Raw Edge Tee', 'Subtly marled for a cool speckled appearance, this short-sleeved tee has raw-cut edges that lend it the feel of a well-worn favorite (minus the whole breaking-it-in bit).', NULL, 'COST Speckled Raw Edge Tee Subtly marled for a cool speckled appearance, this short-sleeved tee has raw-cut edges that lend it the feel of a well-worn favorite (minus the whole breaking-it-in bit).', '2015-03-24 12:52:55', '2015-03-24 12:52:55', NULL, '1427172775_COST Speckled Raw Edge Tee.jpg'),
(29, 'COST Mesh-Paneled Colorblock Tee', 'We added panels of sporty athletic mesh and stripes of graphic colorblocking to this laid-back short-sleeved tee.', NULL, 'COST Mesh-Paneled Colorblock Tee We added panels of sporty athletic mesh and stripes of graphic colorblocking to this laid-back short-sleeved tee.', '2015-03-24 12:54:32', '2015-03-24 12:54:32', NULL, '1427172872_COST Mesh-Paneled Colorblock Tee.jpg'),
(30, 'COST Striped Round-Hem', 'A mix of solid panels and high-impact stripes give this short-sleeved COST its sporty feel. A rounded hem and a just-right hint of slouch provide its easy air of cool.', NULL, 'COST Striped Round-Hem A mix of solid panels and high-impact stripes give this short-sleeved COST its sporty feel. A rounded hem and a just-right hint of slouch provide its easy air of cool.', '2015-03-24 12:59:42', '2015-03-24 12:59:42', NULL, '1427173182_COST Striped Round-Hem.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `selling_prices`
--

CREATE TABLE IF NOT EXISTS `selling_prices` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selling_prices`
--

INSERT INTO `selling_prices` (`id`, `product_id`, `price`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 800.00, '2015-03-21 09:51:50', '2015-03-21 11:43:29', NULL, NULL),
(2, 2, 800.00, '2015-03-23 06:17:08', '2015-03-27 23:49:35', NULL, NULL),
(3, 3, 1200.00, '2015-03-23 06:28:13', '2015-03-23 06:28:13', NULL, NULL),
(4, 4, 1299.00, '2015-03-23 07:32:11', '2015-03-23 07:32:11', NULL, NULL),
(5, 5, 999.00, '2015-03-23 07:34:17', '2015-03-23 07:34:17', NULL, NULL),
(6, 6, 1750.00, '2015-03-23 07:39:15', '2015-03-23 07:39:15', NULL, NULL),
(7, 7, 1299.00, '2015-03-23 07:42:10', '2015-03-23 07:42:10', NULL, NULL),
(8, 8, 1299.00, '2015-03-23 07:44:25', '2015-03-23 07:44:25', NULL, NULL),
(9, 9, 1300.00, '2015-03-23 07:48:10', '2015-03-23 07:48:10', NULL, NULL),
(10, 10, 2250.00, '2015-03-23 07:49:41', '2015-03-23 07:49:41', NULL, NULL),
(11, 11, 799.00, '2015-03-23 07:54:46', '2015-03-23 07:54:46', NULL, NULL),
(12, 12, 2950.00, '2015-03-23 07:58:42', '2015-03-23 07:58:42', NULL, NULL),
(13, 13, 2500.00, '2015-03-23 08:04:19', '2015-03-23 08:04:19', NULL, NULL),
(14, 14, 1099.00, '2015-03-23 08:08:14', '2015-03-23 08:08:14', NULL, NULL),
(15, 15, 1900.00, '2015-03-23 16:26:01', '2015-03-23 16:26:01', NULL, NULL),
(16, 16, 645.00, '2015-03-23 16:32:36', '2015-03-23 16:32:36', NULL, NULL),
(17, 17, 900.00, '2015-03-23 16:38:54', '2015-03-23 16:38:54', NULL, NULL),
(18, 18, 780.00, '2015-03-23 16:49:08', '2015-03-23 16:49:08', NULL, NULL),
(19, 19, 899.00, '2015-03-23 16:51:29', '2015-03-23 16:51:29', NULL, NULL),
(20, 20, 799.00, '2015-03-23 16:55:24', '2015-03-23 16:55:24', NULL, NULL),
(21, 21, 699.00, '2015-03-23 16:58:07', '2015-03-23 16:58:07', NULL, NULL),
(22, 22, 800.00, '2015-03-23 21:26:08', '2015-03-23 21:26:08', NULL, NULL),
(23, 23, 1999.00, '2015-03-23 21:29:14', '2015-03-23 21:29:14', NULL, NULL),
(24, 24, 2000.00, '2015-03-23 22:21:04', '2015-03-23 22:21:04', NULL, NULL),
(25, 25, 1999.00, '2015-03-24 12:44:52', '2015-03-24 12:44:52', NULL, NULL),
(26, 26, 699.00, '2015-03-24 12:47:46', '2015-03-24 12:47:46', NULL, NULL),
(27, 27, 599.00, '2015-03-24 12:50:40', '2015-03-24 12:50:40', NULL, NULL),
(28, 28, 899.00, '2015-03-24 12:52:55', '2015-03-24 12:52:55', NULL, NULL),
(29, 29, 699.00, '2015-03-24 12:54:33', '2015-03-24 12:54:33', NULL, NULL),
(30, 30, 899.00, '2015-03-24 12:59:42', '2015-03-24 12:59:42', NULL, NULL),
(31, 1, 800.00, '2015-03-27 23:47:47', '2015-03-27 23:47:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stocks` int(100) NOT NULL,
  `Small` int(11) DEFAULT NULL,
  `Large` int(11) DEFAULT NULL,
  `XLarge` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `stocks`, `Small`, `Large`, `XLarge`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 42, -2, 20, 24, '2015-03-21 09:51:50', '2015-03-21 11:43:29', NULL, NULL),
(2, 2, 61, 21, 20, 20, '2015-03-23 06:17:08', '2015-03-27 23:49:35', NULL, NULL),
(3, 3, 60, 20, 20, 20, '2015-03-23 06:28:12', '0000-00-00 00:00:00', NULL, NULL),
(4, 4, 60, 20, 20, 20, '2015-03-23 07:32:11', '0000-00-00 00:00:00', NULL, NULL),
(5, 5, 60, 20, 20, 20, '2015-03-23 07:34:17', '0000-00-00 00:00:00', NULL, NULL),
(6, 6, 60, 20, 20, 20, '2015-03-23 07:39:15', '0000-00-00 00:00:00', NULL, NULL),
(7, 7, 60, 20, 20, 20, '2015-03-23 07:42:10', '0000-00-00 00:00:00', NULL, NULL),
(8, 8, 60, 20, 20, 20, '2015-03-23 07:44:25', '0000-00-00 00:00:00', NULL, NULL),
(9, 9, 60, 20, 20, 20, '2015-03-23 07:48:10', '0000-00-00 00:00:00', NULL, NULL),
(10, 10, 60, 20, 20, 20, '2015-03-23 07:49:41', '0000-00-00 00:00:00', NULL, NULL),
(11, 11, 60, 20, 20, 20, '2015-03-23 07:54:46', '0000-00-00 00:00:00', NULL, NULL),
(12, 12, 60, 20, 20, 20, '2015-03-23 07:58:42', '0000-00-00 00:00:00', NULL, NULL),
(13, 13, 60, 20, 20, 20, '2015-03-23 08:04:19', '0000-00-00 00:00:00', NULL, NULL),
(14, 14, 60, 20, 20, 20, '2015-03-23 08:08:13', '0000-00-00 00:00:00', NULL, NULL),
(15, 15, 60, 20, 20, 20, '2015-03-23 16:26:00', '0000-00-00 00:00:00', NULL, NULL),
(16, 16, 60, 20, 20, 20, '2015-03-23 16:32:36', '0000-00-00 00:00:00', NULL, NULL),
(17, 17, 60, 20, 20, 20, '2015-03-23 16:38:54', '0000-00-00 00:00:00', NULL, NULL),
(18, 18, 60, 20, 20, 20, '2015-03-23 16:49:08', '0000-00-00 00:00:00', NULL, NULL),
(19, 19, 60, 20, 20, 20, '2015-03-23 16:51:29', '0000-00-00 00:00:00', NULL, NULL),
(20, 20, 60, 20, 20, 20, '2015-03-23 16:55:24', '0000-00-00 00:00:00', NULL, NULL),
(21, 21, 60, 20, 20, 20, '2015-03-23 16:58:07', '0000-00-00 00:00:00', NULL, NULL),
(22, 22, 60, 20, 20, 20, '2015-03-23 21:26:08', '0000-00-00 00:00:00', NULL, NULL),
(23, 23, 60, 20, 20, 20, '2015-03-23 21:29:14', '0000-00-00 00:00:00', NULL, NULL),
(24, 24, 60, 20, 20, 20, '2015-03-23 22:21:04', '0000-00-00 00:00:00', NULL, NULL),
(25, 25, 60, 20, 20, 20, '2015-03-24 12:44:52', '0000-00-00 00:00:00', NULL, NULL),
(26, 26, 60, 20, 20, 20, '2015-03-24 12:47:45', '0000-00-00 00:00:00', NULL, NULL),
(27, 27, 60, 20, 20, 20, '2015-03-24 12:50:40', '0000-00-00 00:00:00', NULL, NULL),
(28, 28, 60, 20, 20, 20, '2015-03-24 12:52:55', '0000-00-00 00:00:00', NULL, NULL),
(29, 29, 60, 20, 20, 20, '2015-03-24 12:54:32', '0000-00-00 00:00:00', NULL, NULL),
(30, 30, 60, 20, 20, 20, '2015-03-24 12:59:42', '0000-00-00 00:00:00', NULL, NULL),
(31, 1, 46, 2, 20, 24, '2015-03-27 23:47:47', '2015-03-27 23:47:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_int` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `email`, `phone_number`, `created`, `modified`, `deleted`, `deleted_int`) VALUES
(9, 'Kanga', 'Imus', 'z@yahoo.com', '0911111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `bank_account` varchar(100) NOT NULL,
  `credit_card` varchar(100) NOT NULL,
  `isactive` int(11) NOT NULL,
  `existflg` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `isactive`, `existflg`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(4, 'g', '$2a$10$eITo.R.sbCnYQubEHtIZRO5dWEqbFs0aTAlV2gR7rEmT8SsbpzLDq', 'g', 'g', 'g', 'g@g.com', 'g@g.com', '', '', '', 1, 1, NULL, NULL, NULL, NULL),
(15, 'Godfrey', '$2a$10$eITo.R.sbCnYQubEHtIZRO5dWEqbFs0aTAlV2gR7rEmT8SsbpzLDq', 'Godfrey', 'Samenta', 'Legaspi', 'Address', 'glsarmenta7@gmail.com', '09178292149', '', '11111111', 1, 1, '2015-03-21 00:38:07', '2015-03-21 00:38:07', NULL, '2015-03-21 12:38:07'),
(16, 'DenmarkTan', '$2a$10$TS/r5LyLOJiCJroNLQ6r1uYcgtbtluQGIRDyeuo8MYZmwu2y9XQMC', 'Denmark Anthony', 'Tan', 'Salvador', 'Obando, Bulacan', 'denmark_tan2002@yahoo.com', '12345678', '', '12345678', 0, 0, '2015-03-21 08:12:49', '2015-03-21 08:12:49', NULL, '2015-03-21 08:12:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emailcodes`
--
ALTER TABLE `emailcodes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurements`
--
ALTER TABLE `procurements`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendations`
--
ALTER TABLE `recommendations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selling_prices`
--
ALTER TABLE `selling_prices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emailcodes`
--
ALTER TABLE `emailcodes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `procurements`
--
ALTER TABLE `procurements`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `recommendations`
--
ALTER TABLE `recommendations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_prices`
--
ALTER TABLE `selling_prices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
