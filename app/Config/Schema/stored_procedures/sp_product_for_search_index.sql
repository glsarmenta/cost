DROP PROCEDURE IF EXISTS sp_product_for_search_index;
DELIMITER //
CREATE PROCEDURE sp_product_for_search_index()
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE p_id INT;
    DECLARE p_name VARCHAR(100);
    DECLARE p_description VARCHAR(2000);
    DECLARE p_view_status TINYINT;
    DECLARE p_file_name VARCHAR(255);
    DECLARE s_selling_price FLOAT(10,2);
    DECLARE idx TEXT;
    DECLARE p_deleted TINYINT;

    DECLARE cur CURSOR FOR 
            SELECT
                    p.id AS p_id
                  , p.name AS p_name
                  , p.description AS p_description
                  , p.file_name AS p_file_name
                  , p.view_status AS p_view_status
                  , p.deleted AS p_deleted
                  , s.price AS s_selling_price
              FROM products p
             INNER JOIN (
                SELECT
                    product_id
                  , price
                  FROM selling_prices
                 GROUP BY product_id
                 ORDER BY created DESC
                ) s
                ON s.product_id = p.id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO
                  p_id
                , p_name
                , p_description 
                , p_file_name
                , p_view_status
                , p_deleted
                , s_selling_price;

            SET idx =
                CONCAT_WS(' '
                , IFNULL(p_name, ' ')
                , IFNULL(p_description, ' ')
                );

            IF done THEN
                LEAVE ins_loop;
            END IF;

            INSERT INTO search_indices (
                  id
                , name
                , description
                , file_name
                , selling_price
                , view_status
                , idx
                , created
                , modified
                , deleted
            ) VALUES (
                  p_id
                , p_name
                , p_description
                , p_file_name
                , s_selling_price
                , p_view_status
                , idx
                , NOW()
                , NOW()
                , p_deleted
            );
        END LOOP;
    CLOSE cur;
END; //
DELIMITER ;

