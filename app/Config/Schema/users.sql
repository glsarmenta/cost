-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2015 at 04:29 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cost`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `bank_account` varchar(100) NOT NULL,
  `credit_card` varchar(100) NOT NULL,
  `isactive` int(11) NOT NULL,
  `existflg` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `isactive`, `existflg`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(4, 'Godfrey', '$2a$10$AkcJXJbCmQ6Itiu1LlbvNugJfQrZHPwn.UlQmJykvsEgdWRQ6sQ9m', 'Godfrey', 'Sarmenta', 'Legaspi', 'Imus', 'g@yahoo.com', '09178292149', '123456', '123456', 1, 1, '2015-02-13 17:11:10', '2015-02-13 17:11:10', NULL, NULL),
(13, 'Godfrey', '$2a$10$Scwfcb6IikilEBign2bTPO58lWpx/Bn/WKSRQKrdeGOAmLhFLsWI.', 'Godfrey', 'cxz', 'Legaspi', 'Address', 'glsarmenta7@gmail.com', '09178292149', '09', '09', 1, 1, '2015-03-18 22:11:01', '2015-03-18 22:11:01', NULL, '2015-03-18 10:11:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
