-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2015 at 03:28 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cost`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emailcodes`
--

CREATE TABLE IF NOT EXISTS `emailcodes` (
`id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `middle_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `bank_account` varchar(100) CHARACTER SET utf8 NOT NULL,
  `credit_card` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `emailcode` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailcodes`
--

INSERT INTO `emailcodes` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `created`, `modified`, `deleted`, `deleted_date`, `emailcode`) VALUES
(4, 'Godfrey', '$2a$10$kGGHEktDULnzs5pnS3mfzO3UT5RRj9eH5DbZvm6MWkOV49dPJ5vHa', 'Godfrey', 'Sarmenta', 'Legaspi', 'Imus', 'g@yahoo.com', '09178292149', '123456', '123456', '2015-02-13 17:11:10', '2015-02-13 17:11:10', NULL, NULL, ''),
(8, 'zxc', '1', 'Godfrey', 'cxz', 'Legaspi', 'Address', 'glsarmenta7@gmail.com', '09178292149', '09', '09', '2015-02-26 22:11:09', '2015-02-26 22:11:09', NULL, '2015-02-26 10:11:09', 'b4241cbe6d64eeab1421080d42664402');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `product_id`, `price_id`, `amount_of_order`, `amount_of_delivered`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 1, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(2, 3, 3, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(3, 2, 2, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(4, 36, 6, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(5, 37, 7, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(6, 38, 8, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(7, 39, 9, 0, 0, '2015-03-07 08:08:24', '2015-03-07 08:08:24', NULL, NULL),
(8, 1, 1, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(9, 1, 1, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(10, 1, 1, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(11, 1, 1, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(12, 1, 1, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(13, 1, 1, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(14, 1, 1, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(15, 3, 3, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(16, 3, 3, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(17, 3, 3, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(18, 3, 3, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(19, 3, 3, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(20, 3, 3, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(21, 3, 3, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(22, 2, 2, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(23, 2, 2, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(24, 2, 2, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(25, 2, 2, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(26, 2, 2, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(27, 2, 2, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(28, 2, 2, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(29, 36, 6, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(30, 36, 6, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(31, 36, 6, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(32, 36, 6, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(33, 36, 6, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(34, 36, 6, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(35, 36, 6, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(36, 37, 7, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(37, 37, 7, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(38, 37, 7, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(39, 37, 7, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(40, 37, 7, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(41, 37, 7, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(42, 37, 7, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(43, 38, 8, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(44, 38, 8, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(45, 38, 8, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(46, 38, 8, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(47, 38, 8, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(48, 38, 8, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(49, 38, 8, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(50, 39, 9, 1, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(51, 39, 9, 3, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(52, 39, 9, 2, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(53, 39, 9, 36, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(54, 39, 9, 37, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(55, 39, 9, 38, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(56, 39, 9, 39, 0, '2015-03-07 09:33:49', '2015-03-07 09:33:49', NULL, NULL),
(57, 1, 1, 2, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(58, 3, 3, 3, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(59, 2, 2, 4, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(60, 36, 6, 5, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(61, 37, 7, 6, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(62, 38, 8, 7, 0, '2015-03-07 09:55:18', '2015-03-07 09:55:18', NULL, NULL),
(63, 1, 1, 2, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(64, 3, 3, 3, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(65, 2, 2, 4, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(66, 36, 6, 5, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(67, 37, 7, 6, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(68, 38, 8, 7, 0, '2015-03-07 09:55:39', '2015-03-07 09:55:39', NULL, NULL),
(69, 1, 1, 111, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(70, 3, 3, 11, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(71, 2, 2, 11, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(72, 36, 6, 12, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(73, 37, 7, 12, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(74, 38, 8, 12, 0, '2015-03-07 10:04:28', '2015-03-07 10:04:28', NULL, NULL),
(75, 1, 1, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(76, 3, 3, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(77, 2, 2, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(78, 36, 6, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(79, 37, 7, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(80, 38, 8, 1, 0, '2015-03-07 10:05:31', '2015-03-07 10:05:31', NULL, NULL),
(81, 1, 1, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(82, 3, 3, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(83, 2, 2, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(84, 36, 6, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(85, 37, 7, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(86, 38, 8, 1, 0, '2015-03-07 10:06:33', '2015-03-07 10:06:33', NULL, NULL),
(87, 1, 1, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(88, 3, 3, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(89, 2, 2, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(90, 36, 6, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(91, 37, 7, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(92, 38, 8, 1, 0, '2015-03-07 10:06:48', '2015-03-07 10:06:48', NULL, NULL),
(93, 1, 1, 1, 0, '2015-03-07 10:09:13', '2015-03-07 10:09:13', NULL, NULL),
(94, 3, 3, 2, 0, '2015-03-07 10:09:13', '2015-03-07 10:09:13', NULL, NULL),
(95, 2, 2, 3, 0, '2015-03-07 10:09:13', '2015-03-07 10:09:13', NULL, NULL),
(96, 36, 6, 4, 0, '2015-03-07 10:09:13', '2015-03-07 10:09:13', NULL, NULL),
(97, 1, 1, 2, 0, '2015-03-07 10:10:49', '2015-03-07 10:10:49', NULL, NULL),
(98, 3, 3, 3, 0, '2015-03-07 10:10:49', '2015-03-07 10:10:49', NULL, NULL),
(99, 2, 2, 4, 0, '2015-03-07 10:10:49', '2015-03-07 10:10:49', NULL, NULL),
(100, 1, 1, 1, 0, '2015-03-07 10:11:04', '2015-03-07 10:11:04', NULL, NULL),
(101, 3, 3, 1, 0, '2015-03-07 10:11:04', '2015-03-07 10:11:04', NULL, NULL),
(102, 2, 2, 1, 0, '2015-03-07 10:11:04', '2015-03-07 10:11:04', NULL, NULL),
(103, 1, 1, 1, 0, '2015-03-07 10:13:13', '2015-03-07 10:13:13', NULL, NULL),
(104, 3, 3, 1, 0, '2015-03-07 10:13:13', '2015-03-07 10:13:13', NULL, NULL),
(105, 2, 2, 1, 0, '2015-03-07 10:13:13', '2015-03-07 10:13:13', NULL, NULL),
(106, 1, 1, 1, 0, '2015-03-07 10:13:52', '2015-03-07 10:13:52', NULL, NULL),
(107, 3, 3, 1, 0, '2015-03-07 10:13:52', '2015-03-07 10:13:52', NULL, NULL),
(108, 2, 2, 1, 0, '2015-03-07 10:13:52', '2015-03-07 10:13:52', NULL, NULL),
(109, 36, 6, 1, 0, '2015-03-07 10:13:52', '2015-03-07 10:13:52', NULL, NULL),
(110, 1, 1, 1, 0, '2015-03-07 10:14:23', '2015-03-07 10:14:23', NULL, NULL),
(111, 3, 3, 1, 0, '2015-03-07 10:14:23', '2015-03-07 10:14:23', NULL, NULL),
(112, 2, 2, 1, 0, '2015-03-07 10:14:23', '2015-03-07 10:14:23', NULL, NULL),
(113, 36, 6, 1, 0, '2015-03-07 10:14:23', '2015-03-07 10:14:23', NULL, NULL),
(114, 1, 1, 1, 0, '2015-03-07 10:24:06', '2015-03-07 10:24:06', NULL, NULL),
(115, 3, 3, 1, 0, '2015-03-07 10:24:06', '2015-03-07 10:24:06', NULL, NULL),
(116, 2, 2, 1, 0, '2015-03-07 10:24:06', '2015-03-07 10:24:06', NULL, NULL),
(117, 36, 6, 1, 0, '2015-03-07 10:24:06', '2015-03-07 10:24:06', NULL, NULL),
(118, 1, 1, 1, 0, '2015-03-07 10:24:48', '2015-03-07 10:24:48', NULL, NULL),
(119, 3, 3, 1, 0, '2015-03-07 10:24:48', '2015-03-07 10:24:48', NULL, NULL),
(120, 2, 2, 1, 0, '2015-03-07 10:24:48', '2015-03-07 10:24:48', NULL, NULL),
(121, 36, 6, 1, 0, '2015-03-07 10:24:48', '2015-03-07 10:24:48', NULL, NULL),
(122, 1, 1, 1, 0, '2015-03-07 10:26:08', '2015-03-07 10:26:08', NULL, NULL),
(123, 3, 3, 1, 0, '2015-03-07 10:26:08', '2015-03-07 10:26:08', NULL, NULL),
(124, 2, 2, 1, 0, '2015-03-07 10:26:08', '2015-03-07 10:26:08', NULL, NULL),
(125, 36, 6, 1, 0, '2015-03-07 10:26:08', '2015-03-07 10:26:08', NULL, NULL),
(126, 1, 1, 1, 0, '2015-03-07 10:26:49', '2015-03-07 10:26:49', NULL, NULL),
(127, 3, 3, 1, 0, '2015-03-07 10:26:49', '2015-03-07 10:26:49', NULL, NULL),
(128, 2, 2, 1, 0, '2015-03-07 10:26:49', '2015-03-07 10:26:49', NULL, NULL),
(129, 36, 6, 1, 0, '2015-03-07 10:26:49', '2015-03-07 10:26:49', NULL, NULL),
(130, 1, 1, 1, 0, '2015-03-07 10:27:06', '2015-03-07 10:27:06', NULL, NULL),
(131, 3, 3, 1, 0, '2015-03-07 10:27:06', '2015-03-07 10:27:06', NULL, NULL),
(132, 2, 2, 1, 0, '2015-03-07 10:27:06', '2015-03-07 10:27:06', NULL, NULL),
(133, 36, 6, 1, 0, '2015-03-07 10:27:06', '2015-03-07 10:27:06', NULL, NULL),
(134, 1, 1, 12, 0, '2015-03-07 10:27:39', '2015-03-07 10:27:39', NULL, NULL),
(135, 3, 3, 13, 0, '2015-03-07 10:27:39', '2015-03-07 10:27:39', NULL, NULL),
(136, 2, 2, 15, 0, '2015-03-07 10:27:39', '2015-03-07 10:27:39', NULL, NULL),
(137, 36, 6, 16, 0, '2015-03-07 10:27:39', '2015-03-07 10:27:39', NULL, NULL),
(138, 1, 1, 16, 0, '2015-03-07 10:27:50', '2015-03-07 10:27:50', NULL, NULL),
(139, 3, 3, 16, 0, '2015-03-07 10:27:50', '2015-03-07 10:27:50', NULL, NULL),
(140, 2, 2, 12, 0, '2015-03-07 10:27:50', '2015-03-07 10:27:50', NULL, NULL),
(141, 36, 6, 16, 0, '2015-03-07 10:27:50', '2015-03-07 10:27:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
`id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount_of_payment` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE IF NOT EXISTS `payment_methods` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `procurements`
--

CREATE TABLE IF NOT EXISTS `procurements` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `product_categories_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `release_date` datetime DEFAULT NULL,
  `reservation_flg` tinyint(4) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_categories_id`, `name`, `description`, `supplier_id`, `file_name`, `release_date`, `reservation_flg`, `view_status`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 2, 'Boys Fire Print T-Shirt', 'Calling all firefighters! To the rescue! This t-shirt sure will be the hit of the coming summer. Crafted from soft cotton jersey this style is comfy to wear all day long and is a great match with any of his favorite shorts.', 1, '', '2015-02-15 00:00:00', NULL, NULL, '2015-02-10 00:00:00', '2015-03-06 22:12:06', NULL, NULL),
(2, 2, 'Land Of Sun T-shirt Off-White', 'A simple tee with a cool print.  This short-sleeve style from COST was crafted from soft cotton jersey and is perfect to wear all year longx.', 1, '', '2015-02-20 00:00:00', NULL, NULL, '2015-02-10 00:00:00', '2015-03-06 22:12:12', NULL, NULL),
(3, 2, 'Boys White Graphic Shirt', 'Classic style + pre-washed look = cool COST boys tee for summer! This classic style was made using the finest fabric and trim available and will continue to gain character with every wash and wear. Made of soft 100% cotton this style is machine washable.', 1, '', '2015-02-20 00:00:00', NULL, NULL, '2015-02-10 00:00:00', '2015-02-25 22:20:58', NULL, NULL),
(36, 2, 'Classic Crew T- Shirt', 'This COST classic fit t-shirt features more room in the body and is made from a new thicker fabric for a more authentic shape.\r\n\r\nâ€¢ 100% cotton\r\nâ€¢  Machine washable\r\n', 1, 'Products4/1425102595_COST Classic Crew T- Shirt.jpg', '2015-02-28 00:00:00', NULL, NULL, '2015-02-28 13:49:55', '0000-00-00 00:00:00', NULL, NULL),
(37, 2, 'Black Zips Skater T-Shirt', 'Our new skater fit t-shirt with gold zip hem sideseams. Our skater fit features dropped shoulders with a longer body length.\r\n\r\nâ€¢   100% cotton\r\nâ€¢  Machine washable\r\n', 1, '1425103247_COST Black Zips Skater T-Shirt.jpg', '2015-02-28 00:00:00', NULL, NULL, '2015-02-28 14:00:47', '0000-00-00 00:00:00', NULL, NULL),
(38, 2, 'Black Roller Crew Neck T- Shirt', 'Our COST roller fit t-shirts feature more room in the body with lower roll sleeves and a longer body length. Ideal if you wanted your plain casual style to have a more trend-led shape. Black roller crew t-shirt.\r\n\r\nâ€¢  50% cotton, 50% polyester\r\nâ€¢    Machine washable\r\n', 2, '1425103342_COST Black Roller Crew Neck T- Shirt.jpg', '2015-02-21 00:00:00', NULL, NULL, '2015-02-28 14:02:22', '0000-00-00 00:00:00', NULL, NULL),
(39, 2, 'MONOCHROME FLORAL LONG SLEEVE RAGLAN T-SHIRT', 'This COST black and white long sleeve raglan t-shirt features floral print arms with chest detailing. As part of our improved fit range this item features slightly more room in the chest and body with a curved hem.\r\n\r\nâ€¢  100% cotton\r\nâ€¢  Machine washable\r\n', 3, '1425103399_COST MONOCHROME FLORAL LONG SLEEVE RAGLAN T-SHIRT.jpg', '2015-02-06 00:00:00', NULL, NULL, '2015-02-28 14:03:19', '0000-00-00 00:00:00', NULL, NULL),
(40, 2, 'BLACK CIRCLE ROLLER FIT T-SHIRT', 'This black COST Bronx design roller fit t-shirt features an eye catching front and back print. Our roller fit features a new and improved shape that includes a classic fitting body with drop shoulders and fixed roll sleeves.\r\n\r\nâ€¢ 100% cotton\r\nâ€¢  Machine washable\r\n', 1, '1425103465_COST BLACK CIRCLE ROLLER FIT T-SHIRT.jpg', '2015-02-11 00:00:00', NULL, NULL, '2015-02-28 14:04:25', '0000-00-00 00:00:00', NULL, NULL),
(41, 2, 'COST TAPESTRY HEM OVERSIZED FIT T-SHIRT', 'Update a casual look with this white COST fit t-shirt featuring an eye catching tapestry fade design. Our skater fit offers an oversized fit with dropped shoulders and should be worn in your regular size for a true representation of the style.\r\n\r\nâ€¢  75% polyester, 25% cotton\r\nâ€¢    Machine washable\r\n', 2, '1425103532_COST TAPESTRY HEM OVERSIZED FIT T-SHIRT.jpg', '2015-02-10 00:00:00', NULL, NULL, '2015-02-28 14:05:32', '0000-00-00 00:00:00', NULL, NULL),
(42, 3, 'COST Girls Striped Ruffle Tunic Striped', 'This striped tunic with short sleeves and frilly white mesh ruffles at the bottom hem is adorable! To create a super cute outfit just add matching leggings. The item is machine washable.\r\nâ€¢   Top 100% Cotton\r\nâ€¢  Contrast 100% Polyester\r\n', 2, '1425110470_COST Girls Striped Ruffle Tunic Striped.jpg', '2015-02-27 00:00:00', NULL, NULL, '2015-02-28 16:01:10', '0000-00-00 00:00:00', NULL, NULL),
(43, 3, 'COST Girls Checked Dress With Sash Pink', 'This pink and white check dress from COST will create a sweet look for your little one. The sleeveless number features white crochet trim at front bodice and brightly colored attached sash. Back zip closure. This dress is machine washable.\r\nâ€¢  100% Cotton\r\n', 3, '1425111086_Girls Checked Dress With Sash Pink.jpg', '2015-02-21 00:00:00', NULL, NULL, '2015-02-28 16:11:26', '0000-00-00 00:00:00', NULL, NULL),
(44, 3, 'COST Girls Ruffled Chiffon Blouse Navy', 'Does it get any prettier than this gorgeous chiffon blouse form COST? Your little princess will love it, as it looks amazing and feels so soft on her gentle skin. And it comes with the interior camisole for modesty. Puffed short sleeves and ruffled trim at the neck and down the front add romantic touch. Machine washable of course.\r\nâ€¢  100% Polyester\r\nâ€¢   Aviator Navy\r\n', 2, '1425111251_Girls Ruffled Chiffon Blouse Navy.jpg', '2015-02-13 00:00:00', NULL, NULL, '2015-02-28 16:14:11', '0000-00-00 00:00:00', NULL, NULL),
(45, 3, 'COST Girls Floral Dress Pink', 'This beautiful floral dress from COST will delight any little girl.  It features textured floral appliques throughout. Zip fastening on the back for easy on and off. Machine washable.\r\nâ€¢ 75% Polyester\r\nâ€¢    15% Viscose\r\nâ€¢  8% Polyamide\r\n', 8, '1425111492_Girls Floral Dress Pink.jpeg', '2015-02-14 00:00:00', NULL, NULL, '2015-02-28 16:18:12', '0000-00-00 00:00:00', NULL, NULL),
(46, 3, 'COST Girls Jeans Jacket', 'This stylish denim jacket from Cost is a perfect for year-round wear. Crafted from cotton / elastane mix it is sure to be a style staple. Little denim bows accent breast pockets. Machine washable.\r\nâ€¢ 98% Cotton\r\nâ€¢   2% Elastane\r\nâ€¢  Dark Denim\r\n', 8, '1425111651_COST Girls Jeans Jacket.jpg', '2015-02-12 00:00:00', NULL, NULL, '2015-02-28 16:20:51', '0000-00-00 00:00:00', NULL, NULL),
(47, 3, 'COST Seamed Pencil Dress (Regular & Petite)', 'Subtle seams accentuate the hourglass silhouette of a sinuous stretch-knit sheath contemporized by a goldtone two-way zipper traveling the full back length.\r\nâ€¢ Exposed back-zip closure.\r\nâ€¢    Unlined.\r\nâ€¢ 65% viscose rayon, 25% nylon, 10% spandex.\r\nâ€¢   Hand wash cold, lay flat to dry.\r\nâ€¢ Dresses.\r\n', 8, '1425112141_Seamed Pencil Dress (Regular & Petite).jpg', '2015-02-13 00:00:00', NULL, NULL, '2015-02-28 16:29:01', '0000-00-00 00:00:00', NULL, NULL),
(48, 3, 'COST Heat Set Dress', 'A heat-set geometric pattern lends complex dimension to this long-sleeve shift dress, while a slender back cutout reveals a bit of skin.\r\nâ€¢ Lined.\r\nâ€¢   100% polyester.\r\nâ€¢  Hand wash cold, dry flat.\r\n', 8, '1425112259_COST Heat Set Dress.jpg', '2015-02-07 00:00:00', NULL, NULL, '2015-02-28 16:30:59', '0000-00-00 00:00:00', NULL, NULL),
(51, 2, 'COST BLACK SLIM LONG LINE FIT T-SHIRT', 'This COST navy textured raglan long sleeve t-shirt is part of the improved fit range that features slightly more room in the body and curved hem.\r\n\r\nâ€¢  50% cotton, 50% polyester\r\nâ€¢    Machine washable\r\n', 3, '1425535842_COST BLACK SLIM LONG LINE FIT T-SHIRT.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 14:10:42', '0000-00-00 00:00:00', NULL, NULL),
(52, 2, 'COST NAVY TEXTURED RAGLAN LONG SLEEVE T-SHIRT', 'This COST navy textured raglan long sleeve t-shirt is part of the improved fit range that features slightly more room in the body and curved hem.\r\n\r\nâ€¢  50% cotton, 50% polyester\r\nâ€¢    Machine washable\r\n', 8, '1425536176_1425535544_COST NAVY TEXTURED RAGLAN LONG SLEEVE T-SHIRT.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 14:16:16', '0000-00-00 00:00:00', NULL, NULL),
(53, 2, 'COST WHITE SLIM LONG LINE FIT T-SHIRT', 'This COST plain white t-shirt is part of our long line range that features a regular fit across the chest and shoulders with an extra-long body length.\r\nâ€¢    100% cotton\r\nâ€¢  Machine washable\r\n', 8, '1425536349_COST WHITE SLIM LONG LINE FIT T-SHIRT.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 14:19:09', '0000-00-00 00:00:00', NULL, NULL),
(54, 2, 'COST BLACK FOIL HEM SKATER FIT PRINT T-SHIRT', 'This COST black skater fit t-shirt features a gold foil placement print that emphasises the garments oversized shape. Our skater fit features an oversized body shape with dropped shoulders and should be worn in your regular size for a true relaxed shape.\r\n\r\nâ€¢  51% viscose, 49% polyester\r\nâ€¢   Machine washable\r\n', 3, '1425536482_COST BLACK FOIL HEM SKATER FIT PRINT T-SHIRT.jpg', '2015-03-01 00:00:00', NULL, NULL, '2015-03-05 14:21:22', '0000-00-00 00:00:00', NULL, NULL),
(55, 2, 'COST BURGUNDY FAUX LEATHER PANEL T-SHIRT', 'This classic COST fit t-shirt has been updated with faux leather and contrast burgundy panel work. Our classic fit fits true to size with more room in the chest slightly longer body length.\r\n\r\nâ€¢   50% cotton, 50% modal\r\nâ€¢    Machine washable\r\n', 3, '1425536706_COST BURGUNDY FAUX LEATHER PANEL T-SHIRT.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 14:25:06', '0000-00-00 00:00:00', NULL, NULL),
(56, 2, 'COST FROST BLACK CONTRAST RAGLAN LONGSLEEVE T-SHIRT', 'This COST black and white marl effect long sleeve t-shirt has been updated with a new and improved fit. The item now features a curved hem, a more relaxed body shape and a skater neck trim.\r\n\r\nâ€¢    100% cotton\r\nâ€¢  Machine washable\r\n', 3, '1425536848_COST FROST BLACK CONTRAST RAGLAN LONGSLEEVE T-SHIRT.jpg', '2015-03-02 00:00:00', NULL, NULL, '2015-03-05 14:27:28', '0000-00-00 00:00:00', NULL, NULL),
(57, 2, 'COST NAVY STRIPED ROLLER T-SHIRT', 'This COST navy and white classic stripe t-shirt is part of our roller fit range that offers lower and fuller stitched roll sleeves with a classic fitting body.\r\n\r\nâ€¢ 100% cotton\r\nâ€¢  Machine washable\r\n', 8, '1425545064_COST NAVY STRIPED ROLLER T-SHIRT.jpg', '2015-03-02 00:00:00', NULL, NULL, '2015-03-05 16:44:24', '0000-00-00 00:00:00', NULL, NULL),
(58, 2, 'COST BURGUNDY/WHITE CONTRAST RAGLAN LONGSLEEVE T-SHIRT', 'This COST burgundy and white raglan long sleeve t-shirt has been updated with a new and improved fit that offers a more relaxed shape, a curved hem and a skater neck trim.\r\n\r\nâ€¢   100% cotton\r\nâ€¢  Machine washable\r\n', 3, '1425545178_COST BURGUNDY WHITE CONTRAST RAGLAN LONGSLEEVE T-SHIRT.jpg', '2015-03-05 00:00:00', NULL, NULL, '2015-03-05 16:46:18', '0000-00-00 00:00:00', NULL, NULL),
(59, 1, 'WASHED FRONT AND BACK T-SHIRT', 'This washed front and back COST print t-shirt is perfect for adding some moody color to your casual look. T-shirt is a classic fit that fits true to size.\r\n\r\nâ€¢ 100% cotton\r\nâ€¢  Machine washable\r\n', 8, '1425545264_WASHED FRONT AND BACK T-SHIRT.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 16:47:44', '0000-00-00 00:00:00', NULL, NULL),
(60, 2, '''New Tech'' COST T-Shirt', 'A logo-branded T-shirt with short raglan sleeves is crafted with COST technology to keep you cool and comfortable.\r\n\r\nâ€¢ Neon colors are brighter in person.\r\nâ€¢  100% polyester.\r\nâ€¢  Machine wash cold, tumble dry low.\r\nâ€¢   Men''s Sportswear.\r\n', 8, '1425545406_''New Tech'' COST T-Shirt.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 16:50:06', '0000-00-00 00:00:00', NULL, NULL),
(61, 1, 'COST Crewneck Jersey T-Shirt', 'A finely textured T-shirt with short sleeves is knit from soft, lightweight jersey.\r\n\r\nâ€¢ 100% combed cotton.\r\nâ€¢  Machine wash cold, tumble dry low.\r\nâ€¢   Made in the Philippines.\r\nâ€¢ Men''s Sportswear.\r\n', 8, '1425545566_COST Trim Fit Crewneck T-Shirt.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 16:52:46', '0000-00-00 00:00:00', NULL, NULL),
(62, 1, 'COST Graphic T-Shirt', 'A relaxed cotton T-shirt features bold text and a COST logo on the chest and back.\r\n\r\nâ€¢  100% cotton.\r\nâ€¢ Machine wash cold, tumble dry low.\r\nâ€¢   Men''s Sportswear.\r\n', 2, '1425545943_COST Graphic T-Shirt.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 16:59:03', '0000-00-00 00:00:00', NULL, NULL),
(63, 1, 'COST Pima Cotton Jersey V-Neck T-Shirt', 'A super soft jersey T-shirt is cut in a regular fit with the COST logo emblem at the chest.\r\nâ€¢   100% pima cotton.\r\nâ€¢    Machine wash warm, line dry.\r\nâ€¢ The Rail\r\n', 2, '1425546086_COST Pima Cotton Jersey V-Neck T-Shirt.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 17:01:26', '0000-00-00 00:00:00', NULL, NULL),
(64, 2, 'COST Trim Fit Crewneck T-Shirt', 'COST Classic short-sleeve T-shirt is cut with a trimmer fit, ideal for layering or wearing on its own.\r\nâ€¢    White and Black are 100% cotton; all other colors are 50% polyester, 38% cotton, 12% rayon.\r\nâ€¢  Machine wash cold, tumble dry low.\r\n', 8, '1425546329_COST Trim Fit Crewneck T-Shirt.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 17:05:29', '0000-00-00 00:00:00', NULL, NULL),
(65, 3, 'COST Print Long Sleeve Shift Dress', 'Sassy pleats and a rusty ombrÃ© print style a groovy long-sleeve shift dress cut with a V-neckline in front and back.\r\n\r\nâ€¢ Back zip closure.\r\nâ€¢    100% polyester.\r\nâ€¢  Dry clean\r\n', 1, '1425548858_COST Print Long Sleeve Shift Dress.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 17:47:38', '0000-00-00 00:00:00', NULL, NULL),
(66, 3, 'COST Ruched Body-Con Tank Dress', 'Gentle ruching at one side adds soft texture to a slinky tulip-hem tank dress.\r\n\r\nâ€¢   Slips on over head.\r\nâ€¢  Unlined.\r\nâ€¢ 56% polyester, 44% cotton.\r\nâ€¢   Machine wash cold, dry flat.\r\n', 8, '1425550146_COST Ruched Body-Con Tank Dress.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:09:06', '0000-00-00 00:00:00', NULL, NULL),
(67, 3, 'COST Racerback Shift Dress', 'A sweet shift dress is simply detailed with a raised seam along the hemline and a sporty racerback.\r\n\r\nâ€¢   34" length (size Medium).\r\nâ€¢    100% polyester.\r\nâ€¢  Machine wash cold, line dry.\r\n', 8, '1425550308_COST Racerback Shift Dress.jpg', '2015-03-02 00:00:00', NULL, NULL, '2015-03-05 18:11:48', '0000-00-00 00:00:00', NULL, NULL),
(68, 3, 'COST Jacquard Knit Fit & Flare Sweater Dress (Regular & Petite)', 'A unique knit jacquardâ€”resembling tiny crocodile headsâ€”freshens a smart sweater-dress cut with barely there cap sleeves and a flared skirt.\r\n\r\nâ€¢  91% rayon, 8% nylon, 1% spandex.\r\nâ€¢ Machine wash cold, tumble dry low.\r\n', 3, '1425550432_COST Jacquard Knit Fit & Flare Sweater Dress (Regular & Petite).jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:13:52', '0000-00-00 00:00:00', NULL, NULL),
(69, 3, 'COST Colorblock A-Line Maxi Dress', 'Bold color-blocked panels style a cool, easygoing maxi dress cut with a sleeveless A-line silhouette that''s perfectly breezy and comfortable.\r\n\r\nâ€¢ Slips on over head with back button-and-loop closure.\r\nâ€¢    95% polyester, 5% spandex.\r\nâ€¢   Machine wash cold, tumble dry low.\r\n', 8, '1425550556_COST Colorblock A-Line Maxi Dress.jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:15:56', '0000-00-00 00:00:00', NULL, NULL),
(70, 3, 'COST Dolman Sleeve Maxi Dress (Regular & Petite)', 'A bateau neckline tops the slouchy dolman-sleeve bodice of an easy jersey maxi dress fashioned with a drawstring waist.\r\nâ€¢ 95% rayon, 5% spandex.\r\nâ€¢   Hand wash cold, dry flat.\r\nâ€¢    Point of View and Petite Focus.\r\n', 8, '1425550966_COST Dolman Sleeve Maxi Dress (Regular & Petite).jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:22:46', '0000-00-00 00:00:00', NULL, NULL),
(71, 3, 'COST Phillipa'' Stripe Ponte Shift Dress', 'Dropped shoulders and a curved hem relax a comfortable ponte shift dress.\r\n\r\nâ€¢   Slips on over head.\r\nâ€¢  Three-quarter sleeves.\r\nâ€¢   Unlined.\r\nâ€¢ 80% polyester, 17% rayon, 3% spandex.\r\nâ€¢    Dry clean or hand wash, dry flat.\r\n', 3, '1425551236_COST Phillipa'' Stripe Ponte Shift Dress.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 18:27:16', '0000-00-00 00:00:00', NULL, NULL),
(72, 3, 'COST Mixed Stripe Ponte Shift Dress (Regular & Petite)', 'Pops of color brighten a crisp ponte shift dress styled with solid elbow-length sleeves and mixed stripes on the front and back.\r\nâ€¢  Exposed back-zip closure.\r\nâ€¢    Unlined.\r\nâ€¢ 95% polyester, 5% spandex with 67% viscose, 28% nylon, 5% spandex contrast.\r\nâ€¢  Dry clean.\r\n', 8, '1425551388_COST Mixed Stripe Ponte Shift Dress (Regular & Petite).jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:29:48', '0000-00-00 00:00:00', NULL, NULL),
(73, 3, 'COST ''Traffik'' Crochet Inset Minidress', 'A swirling Summer of Love minidress is traced by sheer gridded and twisted trim and finished with flower-child flared sleeves.\r\nâ€¢  Back keyhole with rouleau-button closure.\r\nâ€¢    100% viscose.\r\nâ€¢    Dry clean or machine wash cold, line dry.\r\n', 8, '1425551538_COST ''Traffik'' Crochet Inset Minidress.jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 18:32:18', '0000-00-00 00:00:00', NULL, NULL),
(74, 3, 'COST Blouson Chiffon Skater Dress ', 'Lovely cutaway shoulders top an airy chiffon dress crafted with a gently bloused elastic waist and a trend-right skater silhouette.\r\n\r\nâ€¢   Neon colors are brighter in person.\r\nâ€¢  Back keyhole closure.\r\nâ€¢    Lined.\r\nâ€¢   100% polyester.\r\n', 8, '1425551791_COST Blouson Chiffon Skater Dress .jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:36:31', '0000-00-00 00:00:00', NULL, NULL),
(75, 3, 'COST Printed Maxi Dress (Regular & Petite) (Nordstrom Exclusive)', 'A global-inspired print wraps the flowing skirt of a laid-back maxi dress. An Empire waist and racerback top enhance the casual vibe.\r\nâ€¢   Double-layered bodice.\r\nâ€¢   95% viscose rayon, 5% spandex.\r\nâ€¢   Hand wash cold, lay flat to dry.\r\n', 1, '1425552100_COST Printed Maxi Dress (Regular & Petite) (Nordstrom Exclusive).jpg', '2015-03-03 00:00:00', NULL, NULL, '2015-03-05 18:41:40', '0000-00-00 00:00:00', NULL, NULL),
(76, 3, 'COST Short Sleeve Knit Maxi Dress (Regular & Petite)', 'A drawstring waist defines the relaxed silhouette of a stretch-knit maxi dress detailed with a V-neckline and a chest patch pocket. A side slit allows a flirty glimpse of leg.\r\nâ€¢ 95% rayon, 5% spandex.\r\nâ€¢   Hand wash cold, dry flat.\r\n', 8, '1425552251_COST Short Sleeve Knit Maxi Dress (Regular & Petite).jpg', '2015-03-02 00:00:00', NULL, NULL, '2015-03-05 18:44:11', '0000-00-00 00:00:00', NULL, NULL),
(77, 3, 'COST Polka Dot Split Neck Dress (Regular & Petite)', 'Bright polka dots add youthful appeal to a silky split-neck dress bloused by a drawstring waist for an easy, hourglass silhouette.\r\nâ€¢    Slips on over head.\r\nâ€¢  Long sleeves with rounded, single-button cuffs.\r\nâ€¢  Belt shown sold separately.\r\nâ€¢  Unlined.\r\nâ€¢ 100% polyester.\r\nâ€¢  Machine wash cold, tumble dry low.\r\n', 8, '1425552486_COST Polka Dot Split Neck Dress (Regular & Petite).jpg', '2015-03-04 00:00:00', NULL, NULL, '2015-03-05 18:48:06', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
`id` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `gender`, `type`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 'Shorts', NULL, NULL, NULL, NULL),
(2, 1, 'T-Shirt', '2015-02-25 12:44:11', '2015-02-25 12:44:11', NULL, NULL),
(3, 2, 'Dress', '2015-02-25 12:45:09', '2015-02-25 12:45:09', NULL, NULL),
(4, 2, 'Pants', '2015-02-25 12:45:09', '2015-02-25 12:45:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `sort_order` tinyint(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `file_name`, `sort_order`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 'Products4/one.jpeg', 1, '2015-02-13 21:20:33', '2015-02-13 21:20:33', NULL, NULL),
(2, 2, 'Products4/two.jpeg', 2, '2015-02-13 21:20:33', '2015-02-13 21:20:33', NULL, NULL),
(3, 3, 'Products4/three.jpeg', 3, '2015-02-13 21:20:33', '2015-02-13 21:20:33', NULL, NULL),
(19, 35, 'Products4/1424873986_love-hina-392524.jpg', 0, '2015-02-25 22:19:46', '0000-00-00 00:00:00', NULL, NULL),
(20, 36, 'Products4/1425102595_COST Classic Crew T- Shirt.jpg', 0, '2015-02-28 13:49:55', '0000-00-00 00:00:00', NULL, NULL),
(21, 37, 'Products4/1425103247_COST Black Zips Skater T-Shirt.jpg', 0, '2015-02-28 14:00:48', '0000-00-00 00:00:00', NULL, NULL),
(22, 38, 'Products4/1425103342_COST Black Roller Crew Neck T- Shirt.jpg', 0, '2015-02-28 14:02:23', '0000-00-00 00:00:00', NULL, NULL),
(23, 39, 'Products4/1425103399_COST MONOCHROME FLORAL LONG SLEEVE RAGLAN T-SHIRT.jpg', 0, '2015-02-28 14:03:19', '0000-00-00 00:00:00', NULL, NULL),
(24, 40, 'Products4/1425103465_COST BLACK CIRCLE ROLLER FIT T-SHIRT.jpg', 0, '2015-02-28 14:04:25', '0000-00-00 00:00:00', NULL, NULL),
(25, 41, 'Products4/1425103532_COST TAPESTRY HEM OVERSIZED FIT T-SHIRT.jpg', 0, '2015-02-28 14:05:32', '0000-00-00 00:00:00', NULL, NULL),
(26, 42, 'Products4/1425110470_COST Girls Striped Ruffle Tunic Striped.jpg', 0, '2015-02-28 16:01:10', '0000-00-00 00:00:00', NULL, NULL),
(27, 43, 'Products4/1425111086_Girls Checked Dress With Sash Pink.jpg', 0, '2015-02-28 16:11:27', '0000-00-00 00:00:00', NULL, NULL),
(28, 44, 'Products4/1425111251_Girls Ruffled Chiffon Blouse Navy.jpg', 0, '2015-02-28 16:14:11', '0000-00-00 00:00:00', NULL, NULL),
(29, 45, 'Products4/1425111492_Girls Floral Dress Pink.jpeg', 0, '2015-02-28 16:18:12', '0000-00-00 00:00:00', NULL, NULL),
(30, 46, 'Products4/1425111651_COST Girls Jeans Jacket.jpg', 0, '2015-02-28 16:20:51', '0000-00-00 00:00:00', NULL, NULL),
(31, 47, 'Products4/1425112141_Seamed Pencil Dress (Regular & Petite).jpg', 0, '2015-02-28 16:29:01', '0000-00-00 00:00:00', NULL, NULL),
(32, 48, 'Products4/1425112259_COST Heat Set Dress.jpg', 0, '2015-02-28 16:30:59', '0000-00-00 00:00:00', NULL, NULL),
(35, 51, 'Products4/1425535842_COST BLACK SLIM LONG LINE FIT T-SHIRT.jpg', 0, '2015-03-05 14:10:43', '0000-00-00 00:00:00', NULL, NULL),
(36, 52, 'Products4/1425536176_1425535544_COST NAVY TEXTURED RAGLAN LONG SLEEVE T-SHIRT.jpg', 0, '2015-03-05 14:16:18', '0000-00-00 00:00:00', NULL, NULL),
(37, 53, 'Products4/1425536349_COST WHITE SLIM LONG LINE FIT T-SHIRT.jpg', 0, '2015-03-05 14:19:10', '0000-00-00 00:00:00', NULL, NULL),
(38, 54, 'Products4/1425536482_COST BLACK FOIL HEM SKATER FIT PRINT T-SHIRT.jpg', 0, '2015-03-05 14:21:23', '0000-00-00 00:00:00', NULL, NULL),
(39, 55, 'Products4/1425536706_COST BURGUNDY FAUX LEATHER PANEL T-SHIRT.jpg', 0, '2015-03-05 14:25:07', '0000-00-00 00:00:00', NULL, NULL),
(40, 56, 'Products4/1425536848_COST FROST BLACK CONTRAST RAGLAN LONGSLEEVE T-SHIRT.jpg', 0, '2015-03-05 14:27:29', '0000-00-00 00:00:00', NULL, NULL),
(41, 57, 'Products4/1425545064_COST NAVY STRIPED ROLLER T-SHIRT.jpg', 0, '2015-03-05 16:44:24', '0000-00-00 00:00:00', NULL, NULL),
(42, 58, 'Products4/1425545178_COST BURGUNDY WHITE CONTRAST RAGLAN LONGSLEEVE T-SHIRT.jpg', 0, '2015-03-05 16:46:19', '0000-00-00 00:00:00', NULL, NULL),
(43, 59, 'Products4/1425545264_WASHED FRONT AND BACK T-SHIRT.jpg', 0, '2015-03-05 16:47:44', '0000-00-00 00:00:00', NULL, NULL),
(44, 60, 'Products4/1425545406_''New Tech'' COST T-Shirt.jpg', 0, '2015-03-05 16:50:07', '0000-00-00 00:00:00', NULL, NULL),
(45, 61, 'Products4/1425545566_COST Trim Fit Crewneck T-Shirt.jpg', 0, '2015-03-05 16:52:46', '0000-00-00 00:00:00', NULL, NULL),
(46, 62, 'Products4/1425545943_COST Graphic T-Shirt.jpg', 0, '2015-03-05 16:59:04', '0000-00-00 00:00:00', NULL, NULL),
(47, 63, 'Products4/1425546086_COST Pima Cotton Jersey V-Neck T-Shirt.jpg', 0, '2015-03-05 17:01:27', '0000-00-00 00:00:00', NULL, NULL),
(48, 64, 'Products4/1425546329_COST Trim Fit Crewneck T-Shirt.jpg', 0, '2015-03-05 17:05:29', '0000-00-00 00:00:00', NULL, NULL),
(49, 65, 'Products4/1425548858_COST Print Long Sleeve Shift Dress.jpg', 0, '2015-03-05 17:47:38', '0000-00-00 00:00:00', NULL, NULL),
(50, 66, 'Products4/1425550146_COST Ruched Body-Con Tank Dress.jpg', 0, '2015-03-05 18:09:07', '0000-00-00 00:00:00', NULL, NULL),
(51, 67, 'Products4/1425550308_COST Racerback Shift Dress.jpg', 0, '2015-03-05 18:11:49', '0000-00-00 00:00:00', NULL, NULL),
(52, 68, 'Products4/1425550432_COST Jacquard Knit Fit & Flare Sweater Dress (Regular & Petite).jpg', 0, '2015-03-05 18:13:52', '0000-00-00 00:00:00', NULL, NULL),
(53, 69, 'Products4/1425550556_COST Colorblock A-Line Maxi Dress.jpg', 0, '2015-03-05 18:15:56', '0000-00-00 00:00:00', NULL, NULL),
(54, 70, 'Products4/1425550966_COST Dolman Sleeve Maxi Dress (Regular & Petite).jpg', 0, '2015-03-05 18:22:47', '0000-00-00 00:00:00', NULL, NULL),
(55, 71, 'Products4/1425551236_COST Phillipa'' Stripe Ponte Shift Dress.jpg', 0, '2015-03-05 18:27:16', '0000-00-00 00:00:00', NULL, NULL),
(56, 72, 'Products4/1425551388_COST Mixed Stripe Ponte Shift Dress (Regular & Petite).jpg', 0, '2015-03-05 18:29:49', '0000-00-00 00:00:00', NULL, NULL),
(57, 73, 'Products4/1425551538_COST ''Traffik'' Crochet Inset Minidress.jpg', 0, '2015-03-05 18:32:18', '0000-00-00 00:00:00', NULL, NULL),
(58, 74, 'Products4/1425551791_COST Blouson Chiffon Skater Dress .jpg', 0, '2015-03-05 18:36:32', '0000-00-00 00:00:00', NULL, NULL),
(59, 75, 'Products4/1425552100_COST Printed Maxi Dress (Regular & Petite) (Nordstrom Exclusive).jpg', 0, '2015-03-05 18:41:40', '0000-00-00 00:00:00', NULL, NULL),
(60, 76, 'Products4/1425552251_COST Short Sleeve Knit Maxi Dress (Regular & Petite).jpg', 0, '2015-03-05 18:44:11', '0000-00-00 00:00:00', NULL, NULL),
(61, 77, 'Products4/1425552486_COST Polka Dot Split Neck Dress (Regular & Petite).jpg', 0, '2015-03-05 18:48:06', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE IF NOT EXISTS `recommendations` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `selling_prices`
--

CREATE TABLE IF NOT EXISTS `selling_prices` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selling_prices`
--

INSERT INTO `selling_prices` (`id`, `product_id`, `price`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 999.00, '2015-02-13 00:00:00', '2015-03-06 22:12:06', NULL, NULL),
(2, 2, 7960000.00, '0000-00-00 00:00:00', '2015-03-06 22:12:12', NULL, NULL),
(3, 3, 999.99, '2015-02-13 00:00:00', '2015-02-25 22:20:58', NULL, NULL),
(5, 35, 111.00, '2015-02-25 22:19:46', '2015-02-25 22:20:51', NULL, NULL),
(6, 36, 1540.00, '2015-02-28 13:49:55', '2015-02-28 13:49:55', NULL, NULL),
(7, 37, 3520.00, '2015-02-28 14:00:47', '2015-02-28 14:00:47', NULL, NULL),
(8, 38, 1408.00, '2015-02-28 14:02:22', '2015-02-28 14:02:22', NULL, NULL),
(9, 39, 3520.00, '2015-02-28 14:03:19', '2015-02-28 14:03:19', NULL, NULL),
(10, 40, 3520.00, '2015-02-28 14:04:25', '2015-02-28 14:04:25', NULL, NULL),
(11, 41, 4440.00, '2015-02-28 14:05:32', '2015-02-28 14:05:32', NULL, NULL),
(12, 42, 64285.00, '2015-02-28 16:01:10', '2015-02-28 16:01:10', NULL, NULL),
(13, 43, 1072.85, '2015-02-28 16:11:27', '2015-02-28 16:11:27', NULL, NULL),
(14, 44, 1072.85, '2015-02-28 16:14:11', '2015-02-28 16:14:11', NULL, NULL),
(15, 45, 1287.55, '2015-02-28 16:18:12', '2015-02-28 16:18:12', NULL, NULL),
(16, 46, 2577.85, '2015-02-28 16:20:51', '2015-02-28 16:20:51', NULL, NULL),
(17, 47, 2577.85, '2015-02-28 16:29:01', '2015-02-28 16:29:01', NULL, NULL),
(18, 48, 3415.34, '2015-02-28 16:30:59', '2015-02-28 16:30:59', NULL, NULL),
(19, 49, 3300.00, '2015-03-05 13:37:19', '2015-03-05 13:37:19', NULL, NULL),
(20, 50, 3300.00, '2015-03-05 13:49:31', '2015-03-05 13:49:31', NULL, NULL),
(21, 49, 3300.00, '2015-03-05 14:00:02', '2015-03-05 14:00:02', NULL, NULL),
(22, 50, 3300.00, '2015-03-05 14:05:44', '2015-03-05 14:05:44', NULL, NULL),
(23, 51, 3300.00, '2015-03-05 14:10:42', '2015-03-05 14:10:42', NULL, NULL),
(24, 52, 3300.00, '2015-03-05 14:16:18', '2015-03-05 14:16:18', NULL, NULL),
(25, 53, 2008.00, '2015-03-05 14:19:09', '2015-03-05 14:19:09', NULL, NULL),
(26, 54, 3096.00, '2015-03-05 14:21:23', '2015-03-05 14:21:23', NULL, NULL),
(27, 55, 4440.00, '2015-03-05 14:25:06', '2015-03-05 14:25:06', NULL, NULL),
(28, 56, 2640.00, '2015-03-05 14:27:29', '2015-03-05 14:27:29', NULL, NULL),
(29, 57, 2640.00, '2015-03-05 16:44:24', '2015-03-05 16:44:24', NULL, NULL),
(30, 58, 2640.00, '2015-03-05 16:46:19', '2015-03-05 16:46:19', NULL, NULL),
(31, 59, 4040.00, '2015-03-05 16:47:44', '2015-03-05 16:47:44', NULL, NULL),
(32, 60, 1185.41, '2015-03-05 16:50:07', '2015-03-05 16:50:07', NULL, NULL),
(33, 61, 2371.76, '2015-03-05 16:52:46', '2015-03-05 16:52:46', NULL, NULL),
(34, 62, 1802.54, '2015-03-05 16:59:04', '2015-03-05 16:59:04', NULL, NULL),
(35, 63, 2348.05, '2015-03-05 17:01:27', '2015-03-05 17:01:27', NULL, NULL),
(36, 64, 1043.48, '2015-03-05 17:05:29', '2015-03-05 17:05:29', NULL, NULL),
(37, 65, 3225.60, '2015-03-05 17:47:38', '2015-03-05 17:47:38', NULL, NULL),
(38, 66, 2656.38, '2015-03-05 18:09:07', '2015-03-05 18:09:07', NULL, NULL),
(39, 67, 2276.89, '2015-03-05 18:11:49', '2015-03-05 18:11:49', NULL, NULL),
(40, 68, 7020.42, '2015-03-05 18:13:52', '2015-03-05 18:13:52', NULL, NULL),
(41, 69, 5597.37, '2015-03-05 18:15:56', '2015-03-05 18:15:56', NULL, NULL),
(42, 70, 3225.60, '2015-03-05 18:22:47', '2015-03-05 18:22:47', NULL, NULL),
(43, 71, 4174.31, '2015-03-05 18:27:16', '2015-03-05 18:27:16', NULL, NULL),
(44, 72, 5597.37, '2015-03-05 18:29:49', '2015-03-05 18:29:49', NULL, NULL),
(45, 73, 2822.40, '2015-03-05 18:32:18', '2015-03-05 18:32:18', NULL, NULL),
(46, 74, 2182.02, '2015-03-05 18:36:32', '2015-03-05 18:36:32', NULL, NULL),
(47, 75, 2577.85, '2015-03-05 18:41:40', '2015-03-05 18:41:40', NULL, NULL),
(48, 76, 3225.60, '2015-03-05 18:44:11', '2015-03-05 18:44:11', NULL, NULL),
(49, 77, 6166.59, '2015-03-05 18:48:06', '2015-03-05 18:48:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stocks` int(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `stocks`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 10, '0000-00-00 00:00:00', '2015-03-06 22:12:06', NULL, NULL),
(2, 3, 11097, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 2, 11, '0000-00-00 00:00:00', '2015-03-06 22:12:12', NULL, NULL),
(8, 35, 12121, '2015-02-25 22:19:46', '2015-02-25 22:20:51', NULL, NULL),
(9, 36, 100, '2015-02-28 13:49:55', '0000-00-00 00:00:00', NULL, NULL),
(10, 37, 100, '2015-02-28 14:00:47', '0000-00-00 00:00:00', NULL, NULL),
(11, 38, 100, '2015-02-28 14:02:22', '0000-00-00 00:00:00', NULL, NULL),
(12, 39, 100, '2015-02-28 14:03:19', '0000-00-00 00:00:00', NULL, NULL),
(13, 40, 100, '2015-02-28 14:04:25', '0000-00-00 00:00:00', NULL, NULL),
(14, 41, 100, '2015-02-28 14:05:32', '0000-00-00 00:00:00', NULL, NULL),
(15, 42, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(16, 43, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(17, 44, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(18, 45, 100, '2015-02-28 16:18:12', '0000-00-00 00:00:00', NULL, NULL),
(19, 46, 99, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(20, 47, 100, '2015-02-28 16:29:01', '0000-00-00 00:00:00', NULL, NULL),
(21, 48, 100, '2015-02-28 16:30:59', '0000-00-00 00:00:00', NULL, NULL),
(22, 49, 100, '2015-03-05 13:37:18', '0000-00-00 00:00:00', NULL, NULL),
(23, 50, 100, '2015-03-05 13:49:31', '0000-00-00 00:00:00', NULL, NULL),
(24, 49, 100, '2015-03-05 14:00:02', '0000-00-00 00:00:00', NULL, NULL),
(25, 50, 100, '2015-03-05 14:05:44', '0000-00-00 00:00:00', NULL, NULL),
(26, 51, 100, '2015-03-05 14:10:42', '0000-00-00 00:00:00', NULL, NULL),
(27, 52, 100, '2015-03-05 14:16:17', '0000-00-00 00:00:00', NULL, NULL),
(28, 53, 100, '2015-03-05 14:19:09', '0000-00-00 00:00:00', NULL, NULL),
(29, 54, 100, '2015-03-05 14:21:22', '0000-00-00 00:00:00', NULL, NULL),
(30, 55, 100, '2015-03-05 14:25:06', '0000-00-00 00:00:00', NULL, NULL),
(31, 56, 100, '2015-03-05 14:27:28', '0000-00-00 00:00:00', NULL, NULL),
(32, 57, 100, '2015-03-05 16:44:24', '0000-00-00 00:00:00', NULL, NULL),
(33, 58, 100, '2015-03-05 16:46:18', '0000-00-00 00:00:00', NULL, NULL),
(34, 59, 100, '2015-03-05 16:47:44', '0000-00-00 00:00:00', NULL, NULL),
(35, 60, 100, '2015-03-05 16:50:07', '0000-00-00 00:00:00', NULL, NULL),
(36, 61, 100, '2015-03-05 16:52:46', '0000-00-00 00:00:00', NULL, NULL),
(37, 62, 100, '2015-03-05 16:59:04', '0000-00-00 00:00:00', NULL, NULL),
(38, 63, 100, '2015-03-05 17:01:27', '0000-00-00 00:00:00', NULL, NULL),
(39, 64, 100, '2015-03-05 17:05:29', '0000-00-00 00:00:00', NULL, NULL),
(40, 65, 100, '2015-03-05 17:47:38', '0000-00-00 00:00:00', NULL, NULL),
(41, 66, 100, '2015-03-05 18:09:07', '0000-00-00 00:00:00', NULL, NULL),
(42, 67, 100, '2015-03-05 18:11:49', '0000-00-00 00:00:00', NULL, NULL),
(43, 68, 100, '2015-03-05 18:13:52', '0000-00-00 00:00:00', NULL, NULL),
(44, 69, 100, '2015-03-05 18:15:56', '0000-00-00 00:00:00', NULL, NULL),
(45, 70, 100, '2015-03-05 18:22:46', '0000-00-00 00:00:00', NULL, NULL),
(46, 71, 100, '2015-03-05 18:27:16', '0000-00-00 00:00:00', NULL, NULL),
(47, 72, 100, '2015-03-05 18:29:49', '0000-00-00 00:00:00', NULL, NULL),
(48, 73, 100, '2015-03-05 18:32:18', '0000-00-00 00:00:00', NULL, NULL),
(49, 74, 100, '2015-03-05 18:36:31', '0000-00-00 00:00:00', NULL, NULL),
(50, 75, 100, '2015-03-05 18:41:40', '0000-00-00 00:00:00', NULL, NULL),
(51, 76, 100, '2015-03-05 18:44:11', '0000-00-00 00:00:00', NULL, NULL),
(52, 77, 100, '2015-03-05 18:48:06', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_int` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `email`, `phone_number`, `created`, `modified`, `deleted`, `deleted_int`) VALUES
(1, 'Godfreys', 'Imus Cavite', 'glsarmenta7@gmail.com', '09123131231', '2015-02-11 00:00:00', '2015-02-26 21:52:28', NULL, NULL),
(2, 'Arif Corporation', 'Bangladesh', 'Arif@yahoo.com', '123-123-123', '2015-02-14 06:51:35', '2015-02-14 06:51:35', NULL, NULL),
(3, 'Kanga Inc.', 'Ivory Coast', 'kanga@inc.com', '12', '2015-02-14 06:51:35', '2015-02-26 21:51:47', NULL, NULL),
(8, 'denmark Tans', 'bulacan', 'g@g.com', '11', '0000-00-00 00:00:00', '2015-02-26 21:46:53', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `bank_account` varchar(100) NOT NULL,
  `credit_card` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(4, 'Godfrey', '$2a$10$kGGHEktDULnzs5pnS3mfzO3UT5RRj9eH5DbZvm6MWkOV49dPJ5vHa', 'Godfrey', 'Sarmenta', 'Legaspi', 'Imus', 'g@yahoo.com', '09178292149', '123456', '123456', '2015-02-13 17:11:10', '2015-02-13 17:11:10', NULL, NULL),
(8, 'zxc', '$2a$10$AkcJXJbCmQ6Itiu1LlbvNugJfQrZHPwn.UlQmJykvsEgdWRQ6sQ9m', 'Godfrey', 'cxz', 'Legaspi', 'Address', 'glsarmenta7@gmail.com', '09178292149', '09', '09', '2015-02-26 22:11:09', '2015-02-26 22:11:09', NULL, '2015-02-26 10:11:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emailcodes`
--
ALTER TABLE `emailcodes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurements`
--
ALTER TABLE `procurements`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendations`
--
ALTER TABLE `recommendations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selling_prices`
--
ALTER TABLE `selling_prices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emailcodes`
--
ALTER TABLE `emailcodes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `procurements`
--
ALTER TABLE `procurements`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `recommendations`
--
ALTER TABLE `recommendations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_prices`
--
ALTER TABLE `selling_prices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

