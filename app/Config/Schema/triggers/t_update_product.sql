DROP TRIGGER IF EXISTS t_update_product;
DELIMITER //
CREATE TRIGGER t_update_product AFTER UPDATE ON products
FOR EACH ROW
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    UPDATE search_indices SET
          name = NEW.name
        , description = NEW.description
        , file_name = NEW.file_name
        , selling_price = selling_price
        , view_status = NEW.view_status
        , idx = idx
        , modified = NOW()
        , deleted = NEW.deleted
     WHERE id = NEW.id;
END; //
DELIMITER ;

