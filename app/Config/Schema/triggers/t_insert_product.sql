DROP TRIGGER IF EXISTS t_insert_product;
DELIMITER //
CREATE TRIGGER t_insert_product AFTER INSERT ON products
FOR EACH ROW
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    INSERT INTO search_indices (
          id
        , name
        , description
        , file_name
        , selling_price
        , view_status
        , idx
        , created
        , modified
        , deleted
    ) VALUES (
          NEW.id
        , NEW.name
        , NEW.description
        , NEW.view_status
        , NEW.file_name
        , selling_price
        , idx
        , NOW()
        , NOW()
        , NEW.deleted
    );
END; //
DELIMITER ;
