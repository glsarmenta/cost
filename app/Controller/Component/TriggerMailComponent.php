<?php
App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class TriggerMailComponent extends Component{

	public $from_mail = 'glsarmenta7@gmail.com';
	//public $from_mail = 'hideshi@Hideshi-MacBook-Air.local';
	public $from_name = 'Godfrey';

	public $subject = array(
		'Registration' => 'Registration',
		'Shopname' => 'Shop Confirmation',
		'ResetPassword' => 'Reset Confirmation',
		'MethodName' => 'MailSubject'
	);

	public function initialize(Controller $controller = NULL) {

			$this->controller = $controller;

	}

	public function RegistrationMail($data){

		$to_mail_user = array();
		$to_name_user = array();
		if(!empty($data)) {
			foreach ($data as $data)
		$ary_vars = array (
		        	'Fullname' => $data['first_name'].' '.$data['last_name'],
		        	'address' => $data['address'],
		        	'mail' => $data['email'],
		        	'emailcode' => $data['emailcode']
		    	);
				
		    	$to_mail = $data['email'];
				$to_name =  $data['first_name'].' '.$data['last_name'];
		    			    		
		    	// sending process
			    $email = new CakeEmail('mail');

			    $sent = $email
			                ->template('template_email_verification', 'default')
			                ->viewVars($ary_vars)
			                ->emailFormat('text')
			                ->from(array($this->from_mail => $this->from_name))
			                ->to(array($to_mail => $to_name))
			                ->subject($this->subject['Registration'])
			                ->send();

           					 //$this->redirect(array('action' => 'index'));
			
		} else {
			return;
		}
			
}
// 	public function EmailConfirmation($data){

// 		$to_mail_user = array();
// 		$to_name_user = array();
// 		if(!empty($data)) {

// 		$ary_vars = array (
// 		        	'shopname' => $data['Shopadmin']['shopname'],
// 		        	'tel' => $data['Shopadmin']['tel'],
// 		        	'mail' => $data['Shopadmin']['mail'],
// 		        	'address' => $data['Shopadmin']['address']
// 		    	);

// 		    	$to_mail = $data['Shopadmin']['mail'];
// 				$to_name = $data['Shopadmin']['shopname'];
// 		    	// sending process
// 			    $email = new CakeEmail('mail');

// 			    $sent = $email
// 			                ->template('template_shopadmin', 'my_layout')
// 			                ->viewVars($ary_vars)
// 			                ->emailFormat('text')
// 			                ->from(array($this->from_mail => $this->from_name))
// 			                ->to(array($to_mail => $to_name))
// 			                ->subject($this->subject['Shopname'])
// 			                ->send();
			
// 		} else {
// 			return;
// 		}
			
// }
// public function PasswordReset($data){

// 		$to_mail_user = array();
// 		$to_name_user = array();
// 		if(!empty($data)) {

// 		$ary_vars = array (
// 					'shopname' => $data['Shopresetting']['id'],
// 		        	'mail' => $data['Shopresetting']['mail'],
// 		        	'hashedemail' => $data['Shopresetting']['resettinghash']
// 		    	);

// 		    	$to_mail = $data['Shopresetting']['mail'];
// 				$to_name = $data['Shopresetting']['shopadmin_id'];
// 		    	// sending process
// 			    $email = new CakeEmail('mail');

// 			    $sent = $email
// 			                ->template('template_resettingpassword', 'my_layout')
// 			                ->viewVars($ary_vars)
// 			                ->emailFormat('text')
// 			                ->from(array($this->from_mail => $this->from_name))
// 			                ->to(array($to_mail => $to_name))
// 			                ->subject($this->subject['ResetPassword'])
// 			                ->send();
			
// 		} else {
// 			return;
// 		}
			
// }
	public function EmailBought($data){

		$to_mail_user = array();
		$to_name_user = array();
		if(!empty($data)) {
	
		// 	{
		// $ary_vars = array (
		// 			'Prodname' => $info['prodName'],
		//         	'Price' =>$info['price'],
		//         	'Total' => $info['Total'],
		//         	'Date' => $info['date']
		//     	);
		// 	}
				
		    	$to_mail = 'costincorporated2014@gmail.com';
				$to_name = 'Cost';
		    	// sending process
			    $email = new CakeEmail('mail');

			    $sent = $email
			               ->template('template_email_bought', 'default')
			                ->viewVars($data)
			                ->emailFormat('text')
			                ->from(array($this->from_mail => $this->from_name))
			                ->to(array($to_mail => $to_name))
			                ->subject('Thank You')
			                ->send();
			
		} else {
			return;
		}

	}
	
}

?>
