<?php
App::uses('AppController', 'Controller');

class TopsController extends AppController {
	public $components =array('Session','TriggerMail');
	public $uses = array('Product','stocks','order_items');
	function beforeFilter() {

		parent::beforeFilter();
		//these are webpages that can view public or no need to login
		$this->Auth->allow('index','male','Clothing','female','addCart','checkout','getCheckout',
			'deleteitem','checkLog','contacts','thanks');
	}

	public function index() {

	}
	//function for male page 
	public function male($id = null){

		$data = $this->Product->find('all',array('conditions'=>array('product_categories.gender'=>1)));
		$this->set('data',$data);



	}
	//function for female page
	public function female($id = null){
		$data = $this->Product->find('all',array('conditions'=>array('product_categories.gender'=>0)));
		$this->set('data',$data);



	}
	//function for adding items to the cart
	public function addCart($id = null){
		$this->autoRender = false;
		$this->layout = false;
		$initial = $this->request->data;
		$a = $this->stocks->find('first', array(
			'conditions' => array('product_id' => $initial['productid'])));

		$data = $this->Session->read('itemCart');
		$data[] = $this->request->data;
		$this->Session->write('itemCart', $data);
		$this->Session->write('fullcart',$data);

		print_r('1');


	}
	public $publicProdId;
	public function contacts(){

	}
	//checking out  removing not valid values and counting 
	public function checkout(){

		$prodId = $this->Session->read('prodId');
		$prodId = array_filter($prodId);
		$prodId = array_unique ($prodId);
		$this->publicProdId = $prodId;
		return count($this->publicProdId);
	}
	public function getItem(){
		if($this->Session->read('prodId') != Null){
			$prodId = $this->Session->read('prodId');
			$prodId = array_filter($prodId);
			$prodId = array_unique($prodId);
			$this->publicProdId = $prodId;
			return $this->publicProdId;    
		}

	}
	//functin for getcheckout (www.cost.com/Tops/getCheckout)
	public function getCheckout(){
		//reading itemCart then save to $items
		$items = $this->Session->read('itemCart');
		$data = array();
		$x=0; //initial count
		if($this->Session->read('itemCart')!=null){
			foreach($items  as $key){
				//saving the fetch data to $data
				$data[] =$this->Product->find('all',array('conditions'=>array('Product.id'=>$key['productid'])));
				//add sizes
				if($key['size']==1){
					$data[$x][0]['SizeName'] = 'Small';    
				}
				if($key['size']==2){
					$data[$x][0]['SizeName'] = 'Large';    
				}
				if($key['size']==3){
					$data[$x][0]['SizeName'] = 'X-Large';    
				}
				$x+=1;

			}

		}

		$this->set('data',$data);

	}
	//delete item from cart using Ajax request
	public function deleteitem($id = null){
		$z = $this->Session->read('itemCart');
		unset($z[$id]);

		//saving new item from session
		print_r($this->Session->read('fullcart'));
		$this->Session->write('itemCart',$z);
		$this->redirect('/Tops/getcheckout');

	}
	//count items print to the cart
	public function checkLog(){
		$this->autoRender = false;
		$this->layout = false;
		Configure::write('debug', 0);

		$numberOfprod = $this->request->data;
		//process for saving all the data and send it to the database 
		if($this->Auth->User('id')!=null)
		{
			if($this->Session->read('itemCart')!= Null){
				$items = $this->Session->read('itemCart');
				$zxc = array();
				foreach($items as $info){
					$zxc[] = $this->Product->find('all',
						array('conditions'=>array('Product.id'=>$info['productid']))) ;  
				}

				$saveitem = array();
				$y=0;

				foreach($zxc as $update) {


					$this->order_items->create();
					$saveitem['product_id'] =$update[0]['Product']['id'];
					$saveitem['price_id']= $update[0]['selling_prices']['id'];
					$saveitem['amount_of_order'] =$numberOfprod['item'][$y];
					$saveitem['amount_of_delivered'] = $this->Auth->User('id');

					if($numberOfprod['sizes'][$y]=='Small'){
						$saveitem['size']= 1; 


					}
					elseif($numberOfprod['sizes'][$y]=='Large'){

						$saveitem['size']= 2;
					}
					elseif($numberOfprod['sizes'][$y]=='X-Large'){
						$saveitem['size']= 3;
					}
					$this->order_items->save($saveitem);
					$y++;


				}
				print_r('1');
				//$this->redirect('/Tops/thanks');

			}

		}
		else{
			print_r('need to login');
		}
	}
	public function checkoutItems(){


	}
	public function debug(){


	}
	public function clothing($id=null){
		//clothing web page populating from request
		$data = $this->Product->find('all',array('conditions'=>array('Product.id'=>$id)));
		$this->set('data',$data);
		if($id == Null){

		}

	}
	public function thank(){
		//send email purchases
		$this->autoRender = false;
		$this->layout = false;
		if($this->Session->read('itemCart')!=null){
			$items = $this->Session->read('itemCart');
			$zxc = array();
			foreach($items as $items){
				$zxc[]  = $this->Product->find('all',
					array('conditions'=>array('Product.id'=>$items['productid']))) ;

			}


			$x = array();
			$y = 0;
			foreach($zxc as $zxc)
				foreach($zxc as $item){
					$x['prodId'][] = $item['Product']['id'];
					$x['prodName'][] = $item['Product']['name'];
					$x['price'][] = $item['selling_prices']['price'];

					$y += $item['selling_prices']['price'];
				}
				$x['date'][]= date("Y-m-d H:i:s");
				$x['Total'][]= $y;

//if the email success
if($this->TriggerMail->EmailBought($x)) {//$this->TriggerMail->EmailBought($x)){
	$this->Session->delete('itemCart');
	$this->redirect('/Tops/thanks');

}
else{
	//deleting session after completing 
	$this->Session->delete('itemCart');

	$this->redirect('/Tops/thanks');
}
$this->redirect('/Tops/thanks');
}


}
public function thanks(){

}



}
