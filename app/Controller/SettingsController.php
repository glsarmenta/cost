<?php
App::uses('AppController', 'Controller');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class SettingsController extends AppController {
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login','registration','index','emailcode','logout');
    }
    //setting the components using TriggerMail and Sessions
    public $components =array('Session','TriggerMail');
    //Using the User and emailcode table
    public $uses = array('User','emailcode');


    //function for login
    public function login(){
        //setting the variable 
        $this->set('logErr','');
         //Login modudle
        $cookie = $this->Cookie->read('remember_me_cookie');

        if($cookie!=null);
        {
            $this->set('email',@$cookie['mail']);
            $this->set('password',@$cookie['password']);
        }
        //if the user post  login
        if($this->request->is(array("post"))){
            //delete current Auth 
            $this->Session->delete('Auth');
            //removing email validation for login purposes
            $this->User->validator()->remove('email', 'isUnique');

            //setting the User validation
            $this->User->set($this->request->data);
            //check if the user input right validation
            if($this->User->validates()){
                //if the user login success
              if ($this->Auth->login()){
                //check if the Auth user is already active and existing in user table
                if($this->Auth->User('isactive')!=0 && $this->Auth->User('existflg')!=0){

                    if($this->request->data['remember']==1){

                        unset($this->request->data['remember']);
                        // hash the user's password
                        $this->Cookie->destroy('remember_me_cookie');
                        $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '183 Days');
                        $this->Cookie->read('remember_me_cookie');
                        //set cookie keys
                        $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
                        $this->Cookie->httpOnly = true;
                        //redirecting if the user id is greater than 4
                        if($this->Auth->User('id')>4){
                            $this->redirect('/Tops/');
                        }
                        //all less than 4 is is Admin
                        else{
                            $this->redirect('/Admins/');   
                        }
                    }
                    else{
                        $this->Cookie->destroy('remember_me_cookie');
                        if($this->Auth->User('id')>4){
                            $this->redirect('/Tops/');
                        }
                        else{
                            $this->redirect('/Admins/');   
                        }
                    }
                }
                else{
                    $this->Session->delete('Auth');
                    $this->set('logErr','Not active');//Invalid Email or password
                }
            }
            else{
                $this->Session->delete('Auth');
                    $this->set('logErr','Invalid Email Or Password');//Invalid Email or password
                }
            }
        }


    }
    //function for registration (www.cost.com/Settings/registration)  
    public function registration(){
        //if the user post information
        if($this->request->is(array("post"))){

            $this->User->set($this->request->data);
            if($this->User->validates()){
                //after the validation meets all the required file the $data variable save array of data
                //for saving in the emailcode table
                $data = array('emailcode'=>array('name'=>$this->request->data['User']['name'],'first_name'=>$this->request->data['User']['first_name'],
                    'last_name'=>$this->request->data['User']['last_name'],'middle_name'=>$this->request->data['User']['middle_name'],
                    'address'=>$this->request->data['User']['address'],'email'=>$this->request->data['User']['email'],
                    'phone_number'=>$this->request->data['User']['phone_number'],
                    'credit_card'=>$this->request->data['User']['credit_card'],'password'=>$this->request->data['User']['password'],
                    'emailcode'=>md5($this->request->data['User']['email']),'deleted_date'=> date("Y-m-d h:i:sa"),
                    'isactive'=>1,'existflg'=>1));
                //opening for save
                $this->emailcode->create();
                //saving the data 
                $this->emailcode->save($data);
                //this function is for saving the registration mail and send an email
                $this->TriggerMail->RegistrationMail($data);
                //redirecting the user to tops/thanks
                return $this->redirect(
                    array('controller' => 'Tops', 'action' => 'thanks'));
            }
        }
        else{
            return false;
        }


    }
    public function index(){


    } 
    //function for logout (www.cost.com/tops/logout)
    public function logout(){
        //removing the current session and move to logout direcint (www.cost.com/Settings/login)
        return $this->redirect($this->Auth->logout());

    }
    //this is for emailcode function (www.cost.com/settings/emailcode)
    public function emailcode($id = null){
        if($this->request->is('post')){
          $data = $this->emailcode->find('all',array('conditions'=>array('emailcode'=>$this->request->query['key'],'email'=>$this->request->data['email'])));
          if($data !=null){
            //the required field is fullname and email address
            $fullname = $this->request->data['name'];
            $dataFullname = $data[0]['emailcode']['first_name'].' '.$data[0]['emailcode']['last_name'];

            if($fullname ==$dataFullname){
                foreach($data as $data)
                    $data['User'] = $data['emailcode'];
                unset($data['emailcode']);
                $this->User->create();
                $this->User->save($data);
                foreach($data as $data)
                    $this->Auth->login($data);
                $this->redirect('/Tops/getcheckout');
            }
        }


    }


}



}
?>