<?php
App::uses('AppController', 'Controller');

class SearchController extends AppController {

    public $components =array('Session');

    public $uses = array('SearchIndex');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }

    public function index() {
        $this->set('words', $this->request->query['words']);
        $words = explode(" ", ltrim(rtrim(mb_ereg_replace("[　]+", " ", $this->request->query['words']), ' '), ' '));
        $conditions = array('AND' => array());
        foreach($words as $word) {
            if(strlen($word) > 1) {
                array_push($conditions['AND'], array('SearchIndex.idx LIKE' => '%'.$word.'%'));
            }
        }
        if(count($conditions['AND']) > 0) {
            $data = $this->SearchIndex->find('all',array('conditions'=>$conditions));
            $this->set('data',$data);
        } else {
            $this->set('data',array());
            $this->Session->setFlash('Please type more than 1 characters.', 'default', array(), 'warn');
        }
    }
}
