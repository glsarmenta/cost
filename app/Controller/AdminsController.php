<?php
App::uses('AppController', 'Controller');
//this class is for Admin Controller (www.cost.com/Admin)
class AdminsController extends AppController {
    //using the available table from database
    public $uses = array('products','suppliers','selling_prices','stock',
        'product_images', 'product_categories','OrderItems');
    public function beforeFilter() {
        parent::beforeFilter();
       
    }
    //setting the limit in pagination 
    public $paginate = array(
        'limit' => 10,
        
    );
    //This function is for Adding products
    public function add(){
        //calling the dashboard template before rendering
       $this->layout = 'dashboard';
       //assigning Session / Auth Id in $userID
        $userID  = $this->Auth->User('id');
        //searching for the suppliers,product categories and setting for view file
       $this->set('supplier',$this->suppliers->find('all'));
       $this->set('product_categories',$this->product_categories->find('all'));

       //calling the model for accepting validation
       $this->products->set($this->request->data);
       $this->set('prodSuc','');

       //if the user post the data for this web page
       if($this->request->is('Post')){
        //checking the validation if the user inputs the correct validation
            if($this->products ->validates()){
                
            //setting the path this user has own folder in webroot directory
            $path=WWW_ROOT. DS . 'files/Products'.$userID;
                 if(!is_dir($path)){
                    mkdir($path);
                        //adding permission to add folder
                      chmod($path, 755);
            }       
            //assigning file name from given input
            $filename = WWW_ROOT. DS . 'files/Products'.$userID.DS.time().'_'.$this->request->data['products']['productsFile']['name']; 
            
            //setting the data from given information of the user
            $data = array('name'=>$this->request->data['products']['name'],
            'product_categories_id'=>$this->request->data['products']['product_categories_id'],
            'description'=>$this->request->data['products']['description'],
            'release_date'=>$this->request->data['products']['release_date'],
            'supplier_id'=>$this->request->data['products']['supplier_id'],
            'created' => date("Y-m-d H:i:s"),

            'file_name'=> time().'_'.$this->request->data['products']['productsFile']['name'] );
            
            //Checking if the user upload a picture
            if($this->data['products']['productsFile']!=Null)
            {   
                //opening the table for inserting 
                $this->products->create();
                //saving the data from the given input to save on the product table
                $this->products->save($data);
                $stock = array('product_id'=>$this->products->getLastInsertID(),
                    'stocks'=>60,
                    'Small'=>20,
                    'Large'=>20,
                    'XLarge'=>20,
                    'created'=>date("Y-m-d H:i:s")
                    );
                //saving stocks 
                $this->stock->create();
                $this->stock->save($stock);
                $selling_prices = array('product_id'=>$this->products->getLastInsertID(),'price'=>$this->request->data['products']['price']);
                $this->selling_prices->create();
                $this->selling_prices->save($selling_prices);
                $this->product_images->create();
                $pi = array('file_name'=>$data['file_name'],'created'=>date("Y-m-d H:i:s"), 'product_id'=>$this->products->getLastInsertID());

                $this->product_images->save($pi);
                 move_uploaded_file($this->data['products']['productsFile']['tmp_name'],$filename);
                 //$this->redirect('/Admins/Add');
                 $this->Session->setFlash('Added product successfuly');
                 $this->set('prodSuc','Added product successfuly');
                 $this->data = null;
             }
             else{
                //setting the error if the validation input the wrong data
                $this->set('prodSuc','Picture is required to post.');
             }
         }
     }
    }

    //function for index page in Admin Controller
    public function index(){
            
    	$this->layout = 'dashboard';
        //populating the data in the view file in index (www.cost.com/Admin/index)
       $data = $this->products->query('Select * from products as products inner join stocks as stocks 
        on products.id = stocks.product_id inner join product_images as pi on pi.product_id = products.id');
        $this->set('data',$data);

    }

    /*
    @param $id - this is product id 
    */
    //function for getting the product using Ajax
    public function get_product($id = null){
        //removing the layout for ajax purposes
        $this->autoRender = false;
        $this->layout = false;
        //Not sanitize query or Manual query for searching the products
        $data = $this->products->query('Select * from products as products inner join stocks as stocks
            on products.id = stocks.product_id inner join selling_prices as selling_prices
            on selling_prices.product_id = products.id where products.name ="'.$id.'"');
        //removing the index [0]
        foreach($data as $data) 
            //sending the respong using JSON to view file
           echo  json_encode ($data);

    }
    public function reports(){
        
    }
    //update product function
    function updateProduct($id = null){
       $this->autoRender = false;
        $this->layout = false;
        $data = $this->products->find('all',array('conditions'=>array('products.id'=>$id)));
        
        foreach($data as $data) 
            $stockitems = $this->request->data['Small'] +  $this->request->data['Large'] + $this->request->data['X-Large'];
        $products = array(
            'id' => $id, 
            'name' => $this->request->data['Productname'],
            'description'=>$this->request->data['Description'],
            
            );
        $stock = array(
            'id'=>$data['stocks']['id'],
            'product_id'=>$id,
            'stocks'=>$stockitems,
            'Small'=>$this->request->data['Small'],
            'Large'=>$this->request->data['Large'],
            'XLarge'=>$this->request->data['X-Large']
            );
        $selling_prices = array(
            'id'=>$data['selling_prices']['id'],
            'product_id'=>$id,
            'price'=>$this->request->data['Price']
            );
        if($this->products->save($products) &&
        $this->stock->save($stock) &&
        $this->selling_prices->save($selling_prices) ){
            print_r('The product has been updated');
        }else{
            print_r('Something went wrong');
        

        }
        



    }

public function purchases(){
    $this->layout = 'dashboard';
    $this->set('orders',$this->OrderItems->check());
    $this->set('TopM',$this->OrderItems->query("Select OrderItems.product_id, SUM(selling_prices.price) as qSum,
                     products.name,product_images.file_name from order_items as OrderItems inner join
                     products as products on OrderItems.product_id = products.id
                     inner join stocks as stocks on stocks.product_id = products.id
                     inner join users as users on OrderItems.amount_of_delivered = users.id
                     inner join selling_prices as selling_prices on selling_prices.product_id = 
                     products.id 
                     inner join product_categories as product_categories on product_categories.id =
                     products.product_categories_id
                     inner join product_images as product_images 
                     on product_images.product_id = products.id
                     Where product_categories.gender =1 

                     GROUP BY OrderItems.product_id LIMIT 2"));
        $this->set('TopF',$this->OrderItems->query("Select OrderItems.product_id, SUM(selling_prices.price) as qSum,
                     products.name,product_images.file_name from order_items as OrderItems inner join
                     products as products on OrderItems.product_id = products.id
                     inner join stocks as stocks on stocks.product_id = products.id
                     inner join users as users on OrderItems.amount_of_delivered = users.id
                     inner join selling_prices as selling_prices on selling_prices.product_id = 
                     products.id 
                     inner join product_categories as product_categories on product_categories.id =
                     products.product_categories_id
                     inner join product_images as product_images 
                     on product_images.product_id = products.id
                                          Where product_categories.gender =0 

                     GROUP BY OrderItems.product_id LIMIT 2"));
}
}
