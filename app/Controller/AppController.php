<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('AuthComponent', 'Controller/Component'); 

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
// class AppController extends Controller {
	
//     public $components = array(
        
//         'Session',
//         'DebugKit.Toolbar',
        
//         'Cookie',
//         'ACL',
//         'Auth' => array(
//         'loginAction' => array(
//             'controller' => 'Settings',
//             'action' => 'login'
            
//         ),
//         'loginRedirect' => array('controller' => 'Admins', 'action' => 'index'),
//             'logoutRedirect' => array('controller' => 'Settings', 'action' => 'login'),
//         'authError' => 'Did you really think you are allowed to see that?',
//         'authenticate' => array(
//             'Form' => array(
//             'userModel' => 'User',               
//                 'passwordHasher' => 'Blowfish',
//                 'fields' => array(
//                   'username' => 'email', //Default is 'username' in the userModel
//                   'password' => 'password'  //Default is 'password' in the userModel
//                 )
//             )
//         )
//     )

//          );
class AppController extends Controller {

    public $components = array(
        'DebugKit.Toolbar',
        'Session',
        
        'Paginator',
        'Cookie',
        
        'Auth' => array(

        'authenticate' => array(
            'Form' => array(

                'passwordHasher' => 'Blowfish',
                'userModel' => 'User',
                'fields' => array(
                    'password' => 'password',
                    'username' => 'email'
                  // /  'password' => 'password'
                ),
                
            )
        )
        ,

        'loginRedirect' => array('controller' => 'Admins', 'action' => 'index'),

        'logoutRedirect' => array('controller' => 'Settings', 'action' => 'login'),
        'authorize' => array('Controller') // Added this line
    )

        );



  public function beforeFilter() {

     
     $this->Auth->fields = array(
    'username' => 'email',
    'password' => 'password'
    );


        }
    public function isAuthorized($user) {
    // Admin can access every action
    if (isset($user['id']) && $user['id'] ==4 ) {
        return true;
    }
    if (isset($user['id']) && $user['id'] >4 ) {
        return true;
    }


    
}
      
}