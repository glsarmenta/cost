<?php
App::uses('AppController', 'Controller');
class SuppliersController extends AppController {
    public $uses = array('Suppliers');
    public function beforeFilter() {
        parent::beforeFilter();
       
    }
    public $paginate = array(
        'limit' => 10,
        
    );
    
    public function index(){
    	$this->layout = 'dashboard';
        $this->set('Update');
    	$this->Suppliers->set($this->request->data);

	    if($this->request->is('post')){
	    	if($this->Suppliers->validates()){
	    		$data = $this->request->data;
	    		$this->Suppliers->create();
	    		$this->Suppliers->save($data);
                $this->set('Update','Supplier Added');
	    	}
	    }

    	
    }
    public function edit(){

    	$this->layout = 'dashboard';
    	$this->Paginator->settings = $this->paginate;
    	$data = $this->Paginator->paginate('Suppliers');
    	$this->set('data', $data);
    	

    }
    public function get_supplier($id = null){
		$this->autoRender = false;
        $this->layout = false;
        $data = $this->Suppliers->find('all',array('conditions'=>array('name'=>$id)));
        //echo $data;
        foreach($data as $data) 

        echo  json_encode ($data);

    }
	public function update_Supplier($id = null){
        
        $this->autoRender = false;
        $this->layout = false;
        $data = $this->Suppliers->find('all',array('conditions'=>array('Suppliers.id'=>$id)));
        foreach($data as $data) 
            
        $Suppliers = array(
            'id' => $id, 
            'name' => $this->request->data['name'],
            'address'=>$this->request->data['address'],
            'phone_number'=>$this->request->data['phone_number'],
            
            );
       
        if($this->Suppliers->save($Suppliers)){
            print_r('The Suppliers has been updated');
        }else{
            print_r('Something went wrong');
        

        }

    }

    public function delete_Supplier($id = null){
        
        $this->autoRender = false;
        $this->layout = false;
        $data = $this->Suppliers->find('all',array('conditions'=>array('Suppliers.id'=>$id)));
        foreach($data as $data) 
            
        $Suppliers = array(
            'id' => $id, 
            'name' => $this->request->data['name'],
            'address'=>$this->request->data['address'],
            'phone_number'=>$this->request->data['phone_number'],
            
            );
       
        if($this->Suppliers->delete($id)){
            print_r('The Suppliers has been deleted');
        }else{
            print_r('Something went wrong');
        

        }

    }}