<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Name is required')//※店舗名を入力してください
            ),
        'first_name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Firstname is required')//※店舗名を入力してください
            ),
        'last_name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Lastname is required')//※店舗名を入力してください
            ),
        'middle_name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Middlename is required')//※店舗名を入力してください
            ),
        'credit_card' => array(
            'mustbenumber' => array(
                'rule' => 'numeric',
                'message' => 'A credit card is number format'),
            'between' => array(
                'rule' => array( 'between', 8, 16),
                'message' => 'number must be 8 to 16'
            )
            ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A email is required'),
            'mustbenumber' => array(
                'rule' => 'email',
                'message' => 'Use correct email format'),
            'isUnique' => array(
                'rule' => 'isUnique',
                'required' => 'create',
                'message' => 'Email is already taken',

                'on' => 'create')
               /* 'last' => false*/
            ),
        'phone_number' => array(
            'mustbenumber' => array(
                'rule' => 'numeric',
                'message' => 'A phonenumber is number format')
            ),
        'address' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A address is required'),//住所を入力してください
            ),
        'homepage' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A homepage is required'),
            ),        
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'),
            'between' => array(
                'rule' => array( 'between', 8, 16),
                'message' => 'Password must be 8 to 16'
            )
            ),
        're_password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Confirm password is required'),//パスワードを入力してください
            'same' => array(
                'rule' => array('equalToField', 'password' ), 
                'message' => 'Both password must be same',
            'between' => array(
                'rule' => array( 'between', 8, 16),
                'message' => 'Password must be 8 to 16'
            ),

        )
    )
        );
    public function equalToField($array, $field) {
        return strcmp($this->data[$this->alias][key($array)], $this->data[$this->alias][$field]) == 0;
    }
    public function beforeSave($options = array()) {

       // print_r(count($this->data[$this->alias]));
        //if($this->data[$this->alias]['password']!=Null)
        if(COUNT($this->data[$this->alias])>=4){
            if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']);
            
            return true;
            }
            else{
                return false;
            }
        }        

    }
    
}