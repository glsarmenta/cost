<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class Product extends AppModel {
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Name is required'),
            'Unique' => array(
                'rule' => 'isUnique',
                'message' => 'Name is already taken'),
            ),
        'description' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Description is required')//※店舗名を入力してください
            ),
        'supplier' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Supplier is required')//※店舗名を入力してください
            ),
        'price' => array(
            'required' => array(
                'rule' => '/^[0-9]{3,}$/i',
                'message' => 'Only integers, minimum 3')
            ),
        // 'file' => array(
        //     'notEmpty' => array(
        //         'rule' => array('notEmpty'),
        //         'message' => 'Picture is required'),
        //     'required' => array(
        //         'rule'=>array('extension',array('gif', 'jpeg', 'png', 'jpg')),
        //         'message'=>'Please supply a valid image.'),
        //     'Limitsize'=>array(
        //         'rule'=>array('fileSize', '<=', '2MB'),
        //         'message'=>'Image must be less than 2MB')
        //     ),
        'release_date' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Release Date is required')//※店舗名を入力してください
            )
      
        );
    public $hasOne = 
        array(  
                'selling_prices'=>array('classname'=>'selling_prices','foreignKey'=>'product_id'),
                'product_images'=>array('classname'=>'product_images','foreignKey'=>'product_id'),
                'stocks'=>array('classname'=>'stocks','foreignKey'=>'product_id'));
    public $belongsTo = 
        array(
                'product_categories'=>array('classname'=>'product_categories','foreignKey'=>'product_categories_id'),
                'suppliers'=>array('classname'=>'suppliers','foreignKey'=>'supplier_id')
                );
    
}
