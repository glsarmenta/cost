<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class OrderItems extends AppModel {
 public $belongsTo = array(
        'products' => array(
            'className' => 'products',
            'foreignKey' => 'product_id'
        )
    );
 public function check(){
 	return $this->query("Select * from order_items as OrderItems inner join
 					 products as products on OrderItems.product_id = products.id
 					 inner join stocks as stocks on stocks.product_id = products.id
 					 inner join users as users on OrderItems.amount_of_delivered = users.id
 					 inner join selling_prices as selling_prices on selling_prices.product_id = 
 					 products.id");
 }
}