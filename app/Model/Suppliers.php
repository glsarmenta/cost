<?php
App::uses('AppModel', 'Model');

class Suppliers extends AppModel {
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Name is required'),
        'Unique' => array(
                'rule' => 'isUnique',
                'message' => 'Email is already taken')
            ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A email is required'),
            'Unique' => array(
                'rule' => 'isUnique',
                'message' => 'Email is already taken'),
            'mustbenumber' => array(
                'rule' => 'email',
                'message' => 'Use correct email format'),
               /* 'last' => false*/
            ),
        'phone_number' => array(
            'mustbenumber' => array(
                'rule' => 'numeric',
                'message' => 'A phonenumber is number format'),

            ),
        'address' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A address is required'),//住所を入力してください
            )   

        );
    
    
}