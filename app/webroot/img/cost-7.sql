-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2015 at 05:31 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cost`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_product_for_search_index`()
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE p_name VARCHAR(100);
    DECLARE p_description VARCHAR(2000);
    DECLARE p_view_status TINYINT;
    DECLARE p_file_name VARCHAR(255);
    DECLARE s_selling_price FLOAT(10,2);
    DECLARE idx TEXT;
    DECLARE p_deleted TINYINT;

    DECLARE cur CURSOR FOR 
            SELECT
                    p.name AS p_name
                  , p.description AS p_description
                  , p.file_name AS p_file_name
                  , p.view_status AS p_view_status
                  , p.deleted AS p_deleted
                  , s.price AS s_selling_price
              FROM products p
             INNER JOIN (
                SELECT
                    product_id
                  , price
                  FROM selling_prices
                 GROUP BY product_id
                 ORDER BY created DESC
                ) s
                ON s.product_id = p.id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO
                  p_name
                , p_description 
                , p_file_name
                , p_view_status
                , p_deleted
                , s_selling_price;

            SET idx =
                CONCAT_WS(' '
                , IFNULL(p_name, ' ')
                , IFNULL(p_description, ' ')
                );

            IF done THEN
                LEAVE ins_loop;
            END IF;

            INSERT INTO search_indices (
                  name
                , description
                , file_name
                , selling_price
                , view_status
                , idx
                , created
                , modified
                , deleted
            ) VALUES (
                  p_name
                , p_description
                , p_file_name
                , s_selling_price
                , p_view_status
                , idx
                , NOW()
                , NOW()
                , p_deleted
            );
        END LOOP;
    CLOSE cur;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emailcodes`
--

CREATE TABLE IF NOT EXISTS `emailcodes` (
`id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `middle_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `bank_account` varchar(100) CHARACTER SET utf8 NOT NULL,
  `credit_card` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `emailcode` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailcodes`
--

INSERT INTO `emailcodes` (`id`, `name`, `password`, `first_name`, `last_name`, `middle_name`, `address`, `email`, `phone_number`, `bank_account`, `credit_card`, `created`, `modified`, `deleted`, `deleted_date`, `emailcode`) VALUES
(14, 'zxc', '111111111', '<script>alert(''Hello''); </script>', '<script>alert(''Hello''); </script>', 'Legaspi', '<script>alert(''Hello''); </script>', 'g@yahoo.com', '1', '', '11111111', '2015-03-20 23:57:14', '2015-03-20 23:57:14', NULL, '2015-03-20 11:57:14', '4379e6d6eb9634f9bce8708b580d5e97');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
`id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount_of_payment` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE IF NOT EXISTS `payment_methods` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `procurements`
--

CREATE TABLE IF NOT EXISTS `procurements` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount_of_order` int(11) NOT NULL,
  `amount_of_delivered` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `product_categories_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `release_date` datetime DEFAULT NULL,
  `reservation_flg` tinyint(4) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_categories_id`, `name`, `description`, `supplier_id`, `file_name`, `release_date`, `reservation_flg`, `view_status`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 'zxc', 'sss', 9, '1426865054_3-02.png', '0000-00-00 00:00:00', NULL, NULL, '2015-03-20 23:24:14', '2015-03-20 23:44:29', NULL, NULL);

--
-- Triggers `products`
--
DELIMITER //
CREATE TRIGGER `t_insert_product` AFTER INSERT ON `products`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    INSERT INTO search_indices (
          id
        , name
        , description
        ,filename
        
        
        , idx
        , created
        , modified
        , deleted
    ) VALUES (
          NEW.id
        , NEW.name
        , NEW.description
        
        , NEW.file_name
        
        , idx
        , NOW()
        , NOW()
        , NEW.deleted
    );
END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `t_update_product` AFTER UPDATE ON `products`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idx TEXT;
    DECLARE selling_price FLOAT(10,2);

    DECLARE cur CURSOR FOR 
            SELECT s.price AS selling_price
              FROM selling_prices s
             WHERE s.product_id = NEW.id
             ORDER BY s.created DESC
             LIMIT 1;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO selling_price;
            IF done THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;

    SET idx =
        CONCAT_WS(' '
        , IFNULL(NEW.name, ' ')
        , IFNULL(NEW.description, ' ')
        );
    
    UPDATE search_indices SET
          name = NEW.name
        , description = NEW.description
        , filename = NEW.file_name
        
        
        , idx = idx
        , modified = NOW()
        , deleted = NEW.deleted
     WHERE id = NEW.id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
`id` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `gender`, `type`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 'T-Shirts', '2015-03-20 22:19:47', '2015-03-20 22:19:47', NULL, '2015-03-20 22:19:47'),
(5, 0, 'Dress', '2015-03-20 22:20:26', '2015-03-20 22:20:26', NULL, '2015-03-20 22:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `sort_order` tinyint(4) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `file_name`, `sort_order`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, '1426865054_3-02.png', 0, '2015-03-20 23:24:14', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE IF NOT EXISTS `recommendations` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `search_indices`
--

CREATE TABLE IF NOT EXISTS `search_indices` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT NULL,
  `idx` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `search_indices`
--

INSERT INTO `search_indices` (`id`, `name`, `description`, `view_status`, `idx`, `created`, `modified`, `deleted`, `filename`) VALUES
(1, 'zxc', 'sss', NULL, 'zxc sss', '2015-03-20 23:24:14', '2015-03-20 23:44:29', NULL, '1426865054_3-02.png'),
(2, '', NULL, NULL, '  ', '2015-03-20 23:30:35', '2015-03-20 23:30:35', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `selling_prices`
--

CREATE TABLE IF NOT EXISTS `selling_prices` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selling_prices`
--

INSERT INTO `selling_prices` (`id`, `product_id`, `price`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 200.00, '2015-03-20 23:24:14', '2015-03-20 23:44:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stocks` int(100) NOT NULL,
  `Small` int(11) DEFAULT NULL,
  `Large` int(11) DEFAULT NULL,
  `X-Large` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `stocks`, `Small`, `Large`, `X-Large`, `created`, `modified`, `deleted`, `deleted_date`) VALUES
(1, 1, 60, 20, 20, 20, '2015-03-20 23:24:14', '2015-03-20 23:44:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_int` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `email`, `phone_number`, `created`, `modified`, `deleted`, `deleted_int`) VALUES
(9, 'Kanga', 'Imus', 'z@yahoo.com', '0911111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `bank_account` varchar(100) NOT NULL,
  `credit_card` varchar(100) NOT NULL,
  `isactive` int(11) NOT NULL,
  `existflg` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emailcodes`
--
ALTER TABLE `emailcodes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurements`
--
ALTER TABLE `procurements`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendations`
--
ALTER TABLE `recommendations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selling_prices`
--
ALTER TABLE `selling_prices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emailcodes`
--
ALTER TABLE `emailcodes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `procurements`
--
ALTER TABLE `procurements`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `recommendations`
--
ALTER TABLE `recommendations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_prices`
--
ALTER TABLE `selling_prices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
